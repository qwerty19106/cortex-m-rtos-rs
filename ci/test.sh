#!/bin/bash
set -euxo pipefail

main() {
    local examples=(
        stack
        queue
        concurrent-stack
        priority-queue
        once-uninit
        array-box
        kernel-start
        single-thread
    )
    local fail_examples=(
        
    )

    cd qemu-tests

    for ex in "${examples[@]}"; do
        cargo run --target $TARGET --example $ex $PROFILE
    done
    for ex in "${fail_examples[@]}"; do
        ! env RUSTFLAGS="-C link-arg=-Tlink.x" cargo run --target $TARGET --example $ex $PROFILE
    done
}

main