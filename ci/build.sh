#!/bin/bash
set -euxo pipefail

main() {
    # Compile crate
    export RUST_BACKTRACE=full
    cargo build --target $TARGET $PROFILE

    # Compile examples
    cd qemu-tests   
    cargo build --target $TARGET --examples $PROFILE
}

main