Сборка:
1) rustup target add __target__
Замените __target__ на свою цель сборки:
- Use thumbv7m-none-eabi for ARM Cortex-M3
- Use thumbv7em-none-eabi for ARM Cortex-M4 and Cortex-M7 (no FPU support)
- Use thumbv7em-none-eabihf for ARM Cortex-M4F and Cortex-M7F (with FPU support)
