#![feature(asm)]
#![no_main]
#![no_std]

use core::sync::atomic::AtomicUsize;

use cortex_m_rt::entry;

use cortex_m_rtos::{os::*, *};
pub use cortex_m_rtos_perf::*;

// To prevent linker errors
static ISR_QUEUE: ISRQueue<4> = ISRQueue::new();
define_system_handlers!(ISR_QUEUE);

#[inline]
fn atomic_usize_inc_limit_perf1(ptr: &AtomicUsize, limit: usize) -> bool {
    let res: usize;

    unsafe {
        asm!("
        1:
            ldrex   {val},[{ptr}]
            bkpt
            cmp     {limit},{val}
            beq     2f
        4:
            add     {val},#1
            bkpt
            strex   {res},{val},[{ptr}]
            cbz     {res},3f
            b       1b
        2:
            bkpt
            clrex
            mov     {res},#1
        3:
        ",
        ptr = in(reg_thumb) ptr,
        limit = in(reg_thumb) limit,
        res = lateout(reg_thumb) res,
        val = out(reg_thumb) _
        );
    }

    res == 0
}

#[inline]
fn atomic_usize_inc_limit_perf2(ptr: &AtomicUsize, limit: usize) -> bool {
    let res: usize;

    unsafe {
        asm!("
            bkpt
        1:
            ldrex   {val},[{ptr}]
            cmp     {limit},{val}
            beq     2f
            add     {val},#1
            strex   {res},{val},[{ptr}]
            cbz     {res},3f
            b       1b
        2:
            clrex
            mov     {res},#1
        3:
            bkpt
        ",
        ptr = in(reg_thumb) ptr,
        limit = in(reg_thumb) limit,
        res = lateout(reg_thumb) res,
        val = out(reg_thumb) _
        );
    }

    res == 0
}

#[inline]
fn atomic_usize_inc_limit_perf3(ptr: &AtomicUsize, limit: usize) -> bool {
    let res: usize;

    unsafe {
        asm!("
            b       2f
        1:
            bkpt                    // Second bkpt
            b       4f              // Break loop!
        2:
            bkpt                    // First bkpt
            ldrex   {val},[{ptr}]
            cmp     {limit},{val}
            beq     3f
            clrex                   // Simulate some interrupt (1 cycle)
            add     {val},#1
            strex   {res},{val},[{ptr}]
            cbz     {res},4f
            b       1b
        3:
            clrex
            mov     {res},#1
        4:
        ",
        ptr = in(reg_thumb) ptr,
        limit = in(reg_thumb) limit,
        res = lateout(reg_thumb) res,
        val = out(reg_thumb) _
        );
    }

    res == 0
}

#[inline]
pub fn atomic_usize_dec_non_zero_perf1(ptr: &AtomicUsize) -> bool {
    let res: usize;

    unsafe {
        asm!("
        1:
            ldrex   {val},[{ptr}]
            bkpt
            cbz     {val},2f
            sub     {val},#1
            bkpt
            strex   {res},{val},[{ptr}]
            cbz     {res},3f
            b       1b
        2:
            bkpt
            clrex
            mov     {res},#1
        3:
        ",
        ptr = in(reg_thumb) ptr,
        res = lateout(reg_thumb) res,
        val = out(reg_thumb) _,
        options(nostack)
        );
    }

    res == 0
}

#[inline]
pub fn atomic_usize_dec_non_zero_perf2(ptr: &AtomicUsize) -> bool {
    let res: usize;

    unsafe {
        asm!("
            bkpt
        1:
            ldrex   {val},[{ptr}]
            cbz     {val},2f
            sub     {val},#1
            strex   {res},{val},[{ptr}]
            cbz     {res},3f
            b       1b
        2:
            clrex
            mov     {res},#1
        3:
            bkpt
        ",
        ptr = in(reg_thumb) ptr,
        res = lateout(reg_thumb) res,
        val = out(reg_thumb) _,
        options(nostack)
        );
    }

    res == 0
}

#[inline]
pub fn atomic_usize_dec_non_zero_perf3(ptr: &AtomicUsize) -> bool {
    let res: usize;

    unsafe {
        asm!("
            b       2f
        1:
            bkpt                    // Second bkpt
            b       4f              // Break loop!
        2:
            bkpt                    // First bkpt
            ldrex   {val},[{ptr}]
            cbz     {val},3f
            clrex                   // Simulate some interrupt (1 cycle)
            sub     {val},#1
            strex   {res},{val},[{ptr}]
            cbz     {res},4f
            b       1b
        3:
            clrex
            mov     {res},#1
        4:
        ",
        ptr = in(reg_thumb) ptr,
        res = lateout(reg_thumb) res,
        val = out(reg_thumb) _,
        options(nostack)
        );
    }

    res == 0
}

#[entry]
fn main() -> ! {
    enable_cyccnt(cp());

    // atomic_usize_inc_limit / atomic_usize_inc_limit_extended
    {
        // NOTE: atomic_usize_inc_limit_extended produce the same asm code

        let a = AtomicUsize::new(4);

        // Critical section
        atomic_usize_inc_limit_perf1(&a, 5); // success / failure attempt
        atomic_usize_inc_limit_perf1(&a, 5); // limit

        // Full duration
        atomic_usize_inc_limit_perf2(&a, 6); // success
        atomic_usize_inc_limit_perf2(&a, 6); // limit
        atomic_usize_inc_limit_perf3(&a, 7); // failure attempt
    }

    // atomic_usize_dec_non_zero
    {
        let a = AtomicUsize::new(1);

        // Critical section
        atomic_usize_dec_non_zero_perf1(&a); // success / failure attempt
        atomic_usize_dec_non_zero_perf1(&a); // limit

        // Full duration
        let a1 = AtomicUsize::new(1);
        atomic_usize_dec_non_zero_perf2(&a1); // success
        atomic_usize_dec_non_zero_perf2(&a1); // limit

        let a2 = AtomicUsize::new(1);
        atomic_usize_dec_non_zero_perf3(&a2); // failure attempt
    }

    exit_success();
}
