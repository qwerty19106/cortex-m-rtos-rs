target remote localhost:50000
monitor halt
load
monitor SWO EnableTarget 0 2250000 1 0
monitor reset

break exit_success
commands
    backtrace
    disconnect
    quit 1
end

break panic_infinity_loop
commands
    backtrace
    disconnect
    quit 2
end

catch signal SIGTRAP
commands
    # Size of 'bkpt' is 2 bytes. Jump it
    set $pc += 2
    # Print next asm instruction
    x/i $pc
    # Print CYCCNT register value
    x /uw 0xe0001004
    # Continue
    continue
end

continue
