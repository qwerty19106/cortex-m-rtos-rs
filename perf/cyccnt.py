
def analyze(gdb_log: str):
    for line in gdb_log.splitlines():
        if not line.startswith("0xe0001004:"):
            continue

        cyccnt = line.split()[1]
        yield int(cyccnt)
