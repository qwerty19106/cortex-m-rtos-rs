#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import subprocess
import shutil
import os
import pathlib
from time import sleep
import socket
import sys
import argparse

from cyccnt import analyze


def run_perf(name: str, target: str, features: list) -> str:
    success = True
    load_log = None
    run_log = None

    try:
        # Flash device
        run_cmd = ['gdb-multiarch',
                   '--nx',
                   '-batch',
                   '--return-child-result',
                   '-x', 'commands.gdb',
                   'target/{}/release/examples/{}'.format(target, name)]

        if args.verbose:
            print("> {}".format(" ".join(run_cmd)))
        run_result = subprocess.run(run_cmd, stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT, encoding="utf-8")

        if not run_result.returncode == 1:
            run_log = run_result.stdout
            success = False
            raise Exception(
                "Test {}: failure. GDB return unexpected code {}. See logs/{}/swo.log for details".format(name, run_result.returncode, name))

        return run_result.stdout

    finally:
        if not success:
            log_dir = "logs/{}".format(name)
            os.mkdir(log_dir)

            # Save load log
            if load_log:
                with open(log_dir + "/load.gdb.log", "w") as f:
                    f.write(load_log)

            # Save GDB log
            if run_log:
                with open(log_dir + "/run.lldb.log", "w") as f:
                    f.write(run_log)


parser = argparse.ArgumentParser()
parser.add_argument('--build-only', action='store_true',
                    help='Build tests only')
parser.add_argument('--verbose', action='store_true',
                    help='Show full commands')
parser.add_argument('device', choices=['stm32f103c8', 'stm32f446re'],
                    help='One of variants: stm32f103c8, stm32f446re')
parser.add_argument('names', nargs='*',
                    help='Run one or more tests by name')
args = parser.parse_args()

# Parse device
device_to_target = {
    "stm32f103c8": "thumbv7m-none-eabi",
    "stm32f446re": "thumbv7em-none-eabihf"
}
device = args.device
target = device_to_target[device]

# Chdir
script_dir = os.path.dirname(os.path.abspath(__file__))
os.chdir(script_dir)

# Load list of package names
with open("perf.json") as f:
    perfs = json.load(f)

if not args.name is None:
    # Find test by name
    matches = [perf for perf in perfs if perf["name"] in args.names]
    if len(matches) == 0:
        print("No test {} found".format(args.name))
        sys.exit(1)

    perfs = matches

# Clear logs
shutil.rmtree("logs", ignore_errors=True)
os.mkdir("logs")

if not args.build_only:
    # Run JLinkGDBServer
    gdb_server_cmd = ['JLinkGDBServerCLExe',
                      '-select', 'usb=000771541653',
                      '-if', 'swd',
                      '-port', '50000',
                      '-telnetport', '50001',
                      '-device', device,
                      '-strict',
                      '-log', 'logs/jlink.log']

    if args.verbose:
        print("> {}".format(" ".join(gdb_server_cmd)))
    gdb_server = subprocess.Popen(
        gdb_server_cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

try:
    # Compile examples
    for perf in perfs:
        name = perf["name"]
        features = perf["features"]

        features.insert(0, device)
        build_cmd = ['cargo',
                     '+nightly',
                     'build',
                     '--target', target,
                     '--release',
                     '--example', name,
                     '--features', ",".join(features)]

        env = os.environ.copy()
        env["RUST_BACKTRACE"] = "full"

        print("Build {}".format(name))
        if args.verbose:
            print("> {}".format(" ".join(build_cmd)))

        res = subprocess.run(build_cmd, env=env)
        res.check_returncode()

    if not args.build_only:
        # Check JLinkGDBServer
        if not gdb_server.poll() is None:
            print("JLinkGDBServer exited unexpectedly. See logs/jlink.log for details.")
            sys.exit(1)

        # Run examples
        results = []
        for perf in perfs:
            gdb_log = run_perf(perf["name"], target, perf["features"])
            cnts = list(analyze(gdb_log))

            res = {
                "name": perf["name"],
                "cnts": cnts,
                "result": []
            }

            try:
                for eval_ in perf["evals"]:
                    cycles = cnts[eval_["second"]] - cnts[eval_["first"]]
                    if "offset" in eval_:
                        cycles -= eval_["offset"]

                    res["result"].append({
                        "desc": eval_["desc"],
                        "cycles": cycles
                    })
            except Exception:
                print(res["name"])
                print(res["cnts"])
                print("Error in calc result cycles")
                raise

            results.append(res)

        for res in results:
            print(res["name"])
            print(res["cnts"])
            for eval_ in res["result"]:
                print(eval_["desc"], eval_["cycles"])


        sys.exit(1)

finally:
    if not args.build_only:
        gdb_server.terminate()
        gdb_server.wait()
