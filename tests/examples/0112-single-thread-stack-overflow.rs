#![no_main]
#![no_std]

use core::sync::atomic::*;

use cortex_m_rt::*;

use cortex_m_rtos::{common::*, os::*};
use cortex_m_rtos_tests::*;

static THREAD: Once<ThreadMemory<(), 16>> = Once::new(ThreadMemory::new());

fn thread_func(_arg: (), token: ThreadToken) -> ! {
    // Rustc add header: push	{r7, lr}

    // Locate array on stack
    // Thus size of array equals:
    // size of stack
    // + stack_check field
    // - header size * 2
    // - stack frame size
    const SIZE: usize = 16 + 1 - 2 * 2 - 8;
    let mut arr = [0u32; SIZE];
    for i in 0..SIZE {
        arr[i] = i as u32;
    }
    compiler_fence(Ordering::SeqCst);

    // Check thread stack
    Thread::stack_check(token);

    // Prevent optimize arr!
    compiler_fence(Ordering::SeqCst);
    for i in 0..SIZE {
        if arr[i] > 5 {
            exit_success();
        }
    }
    
    exit_success();
}

#[entry]
fn main() -> ! {
    let main_token = MainToken::new();

    KERNEL.start(cp(), main_token, |_, token| {
        let thread = THREAD.get().to_thread(Priority::Normal, thread_func, ());
        KERNEL.add_thread(thread, token);
    });
}
