#![no_main]
#![no_std]

use cortex_m_rt::*;

use cortex_m_rtos::{common::*, os::*};
use cortex_m_rtos_tests::*;

static THREAD: Once<ThreadMemory<(), 16>> = Once::new(ThreadMemory::new());

fn thread_func(_arg: (), _token: ThreadToken) -> ! {
    panic!("Panic in thread");
}

#[entry]
fn main() -> ! {
    let main_token = MainToken::new();

    KERNEL.start(cp(), main_token, |_, token| {
        let thread = THREAD.get().to_thread(Priority::Normal, thread_func, ());
        KERNEL.add_thread(thread, token);
    });
}
