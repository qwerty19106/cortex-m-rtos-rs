#![no_main]
#![no_std]

use core::sync::atomic::{AtomicUsize, Ordering};

use cortex_m::peripheral::NVIC;
use cortex_m_rt::*;

use cortex_m_rtos::{common::*, os::*, primitive::isr_to_thread::sa_semaphore::*};

use cortex_m_rtos_tests::{
    hal::{prelude::*, stm32::*, timer::*},
    *,
};

#[cfg(feature = "stm32f103c8")]
type TimContext = CountDownTimer<TIM2>;

#[cfg(feature = "stm32f446re")]
type TimContext = Timer<TIM2>;

static TIM_CONTEXT: ISRContext<TimContext, { Interrupt::TIM2 as u8 }> = ISRContext::new();

type Semaphore = SASemaphore<MultiRelease, 4>;
static SEMAPHORE: Semaphore = SASemaphore::new(0);

#[interrupt]
fn TIM2() {
    static mut COUNTER: u32 = 0;

    const TOKEN: ISRTokenNumber =
        unsafe { ISRTokenNumber::new_unchecked_const(Interrupt::TIM2 as u8) };
    let context = TIM_CONTEXT.get::<TOKEN>();
    let timer = context.unwrap();

    // First clear interrupt flag.
    // NOTE! Clear interrupt flag at the end of interrupt can cause repeated TIM2 handler!
    #[cfg(feature = "stm32f103c8")]
    timer.clear_update_interrupt_flag();

    #[cfg(feature = "stm32f446re")]
    timer.clear_interrupt(Event::TimeOut);

    SEMAPHORE.release(TOKEN.into()).unwrap();
    if *COUNTER == 1 {
        SEMAPHORE.release(TOKEN.into()).unwrap();
    }

    *COUNTER += 1;
}

static THREAD: Once<ThreadMemory<(), 256>> = Once::new(ThreadMemory::new());

fn thread_func(_: (), token: ThreadToken) -> ! {
    let acquire = SEMAPHORE.get_acquire();

    // Wait semaphore
    acquire.acquire(token);
    acquire.acquire(token);
    acquire.acquire(token);

    exit_success();
}

#[entry]
fn main() -> ! {
    let main_token = MainToken::new();

    KERNEL.start(
        cp(),
        main_token,
        |_, token| {
            let dp = dp();

            #[cfg(feature = "stm32f103c8")]
            let timer = {
                let mut flash = dp.FLASH.constrain();
                let mut rcc = dp.RCC.constrain();

                // Freeze the configuration of all the clocks in the system and store
                // the frozen frequencies in `clocks`
                let clocks = rcc.cfgr.freeze(&mut flash.acr);

                // Configure the TIM2 timer to trigger an update with 100Hz
                #[cfg(feature = "stm32f103c8")]
                let mut timer =
                    Timer::tim2(dp.TIM2, &clocks, &mut rcc.apb1).start_count_down(100.hz());
                timer.listen(Event::Update);
                timer
            };

            #[cfg(feature = "stm32f446re")]
            let timer = {
                let rcc = dp.RCC.constrain();

                // Freeze the configuration of all the clocks in the system and store
                // the frozen frequencies in `clocks`
                let clocks = rcc.cfgr.freeze();

                // Configure the TIM2 timer to trigger an update with 100Hz
                let mut timer = Timer::tim2(dp.TIM2, 100.hz(), clocks);
                timer.listen(Event::TimeOut);
                timer
            };

            // Set TIM2 context
            TIM_CONTEXT.set(timer, token);

            // Allow TIM2 interrupt
            // TODO: drop unsafe
            unsafe {
                NVIC::unmask(Interrupt::TIM2);
            }

            let thread = THREAD.get().to_thread(Priority::Normal, thread_func, ());

            KERNEL.add_thread(thread, token);

            SEMAPHORE.hooks.before_release.set(before_release, token);
            SEMAPHORE.hooks.after_release.set(after_release, token);
            SEMAPHORE
                .hooks
                .before_pendsv_process
                .set(before_pendsv_process, token);
            SEMAPHORE
                .hooks
                .after_pendsv_process
                .set(after_pendsv_process, token);
            SEMAPHORE
                .hooks
                .before_svc_acquire
                .set(before_svc_acquire, token);
            SEMAPHORE
                .hooks
                .after_svc_acquire
                .set(after_svc_acquire, token);
            SEMAPHORE.hooks.before_acquire.set(before_acquire, token);
            SEMAPHORE.hooks.after_acquire.set(after_acquire, token);
        },
    );
}

fn before_release(counter: &AtomicUsize) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [0, 0, 1];

    assert_eq!(check[index], counter.load(Ordering::SeqCst));

    unsafe {
        COUNTER = index + 1;
    }
}

fn after_release(counter: &AtomicUsize, res: &Result<(), ReleaseError>) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [(1, Ok(())), (1, Ok(())), (2, Ok(()))];

    assert_eq!(check[index].0, counter.load(Ordering::SeqCst));
    assert_eq!(check[index].1, *res);

    unsafe {
        COUNTER = index + 1;
    }
}

fn before_pendsv_process(pending_thread: &Option<Own<Thread>>) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [true, true, false];

    assert_eq!(check[index], pending_thread.is_some());

    unsafe {
        COUNTER = index + 1;
    }
}

fn after_pendsv_process(pending_thread: &Option<Own<Thread>>) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [false, false, false];

    assert_eq!(check[index], pending_thread.is_some());

    unsafe {
        COUNTER = index + 1;
    }
}

fn before_svc_acquire(counter: &AtomicUsize, pending_thread: &Option<Own<Thread>>) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [(0, false), (0, false), (1, false)];

    assert_eq!(check[index].0, counter.load(Ordering::SeqCst));
    assert_eq!(check[index].1, pending_thread.is_some());

    unsafe {
        COUNTER = index + 1;
    }
}

fn after_svc_acquire(counter: &AtomicUsize, pending_thread: &Option<Own<Thread>>) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [(0, true), (0, true), (1, false)];

    assert_eq!(check[index].0, counter.load(Ordering::SeqCst));
    assert_eq!(check[index].1, pending_thread.is_some());

    unsafe {
        COUNTER = index + 1;
    }
}

fn before_acquire(counter: &AtomicUsize) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [0, 0, 1];

    assert_eq!(check[index], counter.load(Ordering::SeqCst));

    unsafe {
        COUNTER = index + 1;
    }
}

fn after_acquire(counter: &AtomicUsize) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [0, 1, 0];

    assert_eq!(check[index], counter.load(Ordering::SeqCst));

    unsafe {
        COUNTER = index + 1;
    }
}
