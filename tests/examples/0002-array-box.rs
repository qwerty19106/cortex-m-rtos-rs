#![no_main]
#![no_std]

use cortex_m_rt::entry;

use cortex_m_rtos::common::*;
pub use cortex_m_rtos_tests::*;

static A: Once<Uninit<[u8; 3]>> = Once::new(Uninit::new());

#[entry]
fn main() -> ! {
    let array = A.get().init([1, 2, 3]);
    let array: ArrayBox<u8> = ArrayBox::new(array);
    assert_eq!(array.size(), 3);
    assert_eq!(array[1], 2);

    // exit from qemu
    exit_success();
}
