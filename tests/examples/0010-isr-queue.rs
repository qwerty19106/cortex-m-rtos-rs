#![no_main]
#![no_std]

use core::sync::atomic::{AtomicUsize, Ordering};

use cortex_m_rt::entry;

use cortex_m_rtos::os::{token::*, *};

use cortex_m_rtos_tests::{hal::stm32::*, *};

struct Event(u32);

impl Event {
    extern "C" fn process_pendsv(&'static self) {}
}

static EVENT1: Event = Event(0);
static EVENT2: Event = Event(1);
static EVENT3: Event = Event(2);
static EVENT4: Event = Event(3);

fn before_push(
    head: &AtomicUsize,
    tail: &AtomicUsize,
    _ring_buffer: &[Option<ISRTask>],
    _task: &ISRTask,
) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [
        (0, 0),
        (0, 1),
        (0, 2),
        (0, 3),
        (usize::MAX, usize::MAX),
        (usize::MAX, 0),
        (usize::MAX, 1),
        (usize::MAX, 2),
    ];

    assert_eq!(check[index].0, head.load(Ordering::SeqCst));
    assert_eq!(check[index].1, tail.load(Ordering::SeqCst));

    unsafe {
        COUNTER = index + 1;
    }
}

fn after_push(
    head: &AtomicUsize,
    tail: &AtomicUsize,
    ring_buffer: &[Option<ISRTask>],
    task: &ISRTask,
    result: &bool,
) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [
        (0, 1, true),
        (0, 2, true),
        (0, 3, true),
        (0, 3, false),
        (usize::MAX, 0, true),
        (usize::MAX, 1, true),
        (usize::MAX, 2, true),
        (usize::MAX, 2, false),
    ];

    let head = head.load(Ordering::SeqCst);
    let tail = tail.load(Ordering::SeqCst);

    assert_eq!(check[index].0, head);
    assert_eq!(check[index].1, tail);
    assert_eq!(check[index].2, *result);

    if *result {
        let old_tail = (tail - 1) & (ISRQueue::SIZE - 1);

        let item = ring_buffer[old_tail];
        assert_eq!(Some(*task), item);
    }

    unsafe {
        COUNTER = index + 1;
    }
}

fn before_run_all(head: &AtomicUsize, tail: &AtomicUsize, _ring_buffer: &[Option<ISRTask>]) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [(0, 3), (usize::MAX, 2)];

    assert_eq!(check[index].0, head.load(Ordering::SeqCst));
    assert_eq!(check[index].1, tail.load(Ordering::SeqCst));

    unsafe {
        COUNTER = index + 1;
    }
}

fn after_run_all(head: &AtomicUsize, tail: &AtomicUsize, _ring_buffer: &[Option<ISRTask>]) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [(3, 3), (2, 2)];

    assert_eq!(check[index].0, head.load(Ordering::SeqCst));
    assert_eq!(check[index].1, tail.load(Ordering::SeqCst));

    unsafe {
        COUNTER = index + 1;
    }
}

fn before_run_task(
    head: &AtomicUsize,
    tail: &AtomicUsize,
    _ring_buffer: &[Option<ISRTask>],
    task: &ISRTask,
) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [
        (1, 3, ISRTask::new(&EVENT1, Event::process_pendsv)),
        (2, 3, ISRTask::new(&EVENT2, Event::process_pendsv)),
        (3, 3, ISRTask::new(&EVENT3, Event::process_pendsv)),
        (0, 2, ISRTask::new(&EVENT1, Event::process_pendsv)),
        (1, 2, ISRTask::new(&EVENT2, Event::process_pendsv)),
        (2, 2, ISRTask::new(&EVENT3, Event::process_pendsv)),
    ];

    assert_eq!(check[index].0, head.load(Ordering::SeqCst));
    assert_eq!(check[index].1, tail.load(Ordering::SeqCst));
    assert_eq!(check[index].2, *task);

    unsafe {
        COUNTER = index + 1;
    }
}

fn after_run_task(
    head: &AtomicUsize,
    tail: &AtomicUsize,
    _ring_buffer: &[Option<ISRTask>],
    _task: &ISRTask,
) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [(1, 3), (2, 3), (3, 3), (0, 2), (1, 2), (2, 2)];

    assert_eq!(check[index].0, head.load(Ordering::SeqCst));
    assert_eq!(check[index].1, tail.load(Ordering::SeqCst));

    unsafe {
        COUNTER = index + 1;
    }
}

fn set_head_tail_to_max(head: &AtomicUsize, tail: &AtomicUsize, _ring_buffer: &[Option<ISRTask>]) {
    head.store(usize::MAX, Ordering::SeqCst);
    tail.store(usize::MAX, Ordering::SeqCst);
}

#[entry]
fn main() -> ! {
    let isr_token: ISRToken = unsafe { ISRTokenNumber::new_unchecked(Interrupt::EXTI0).into() };
    let pendsv_token = unsafe { PendSVToken::new_unchecked() };
    let starting_token = unsafe { KernelStartingToken::new_unchecked() };

    ISR_QUEUE.hooks.before_push.set(before_push, starting_token);
    ISR_QUEUE.hooks.after_push.set(after_push, starting_token);
    ISR_QUEUE
        .hooks
        .before_run_all
        .set(before_run_all, starting_token);
    ISR_QUEUE
        .hooks
        .after_run_all
        .set(after_run_all, starting_token);
    ISR_QUEUE
        .hooks
        .before_run_task
        .set(before_run_task, starting_token);
    ISR_QUEUE
        .hooks
        .after_run_task
        .set(after_run_task, starting_token);

    // Simple test
    assert!(ISR_QUEUE.push(ISRTask::new(&EVENT1, Event::process_pendsv), isr_token));
    assert!(ISR_QUEUE.push(ISRTask::new(&EVENT2, Event::process_pendsv), isr_token));
    assert!(ISR_QUEUE.push(ISRTask::new(&EVENT3, Event::process_pendsv), isr_token));
    assert!(!ISR_QUEUE.push(ISRTask::new(&EVENT4, Event::process_pendsv), isr_token));

    ISR_QUEUE.run_all(pendsv_token);
    // Test with usize overflow
    ISR_QUEUE
        .hooks
        .before_run_all
        .set(set_head_tail_to_max, starting_token);
    ISR_QUEUE
        .hooks
        .after_run_all
        .set(set_head_tail_to_max, starting_token);
    ISR_QUEUE.run_all(pendsv_token);
    ISR_QUEUE
        .hooks
        .before_run_all
        .set(before_run_all, starting_token);
    ISR_QUEUE
        .hooks
        .after_run_all
        .set(after_run_all, starting_token);

    assert!(ISR_QUEUE.push(ISRTask::new(&EVENT1, Event::process_pendsv), isr_token));
    assert!(ISR_QUEUE.push(ISRTask::new(&EVENT2, Event::process_pendsv), isr_token));
    assert!(ISR_QUEUE.push(ISRTask::new(&EVENT3, Event::process_pendsv), isr_token));
    assert!(!ISR_QUEUE.push(ISRTask::new(&EVENT4, Event::process_pendsv), isr_token));

    ISR_QUEUE.run_all(pendsv_token);

    // exit by itm
    exit_success();
}
