#![no_main]
#![no_std]

use cortex_m_rt::entry;

use cortex_m_rtos::common::*;
pub use cortex_m_rtos_tests::*;

static A: Once<u8> = Once::new(0);

#[entry]
fn main() -> ! {
    A.get();
    A.get(); // Failed!

    // exit by itm
    exit_success();
}
