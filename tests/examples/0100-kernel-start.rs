#![no_main]
#![no_std]

use cortex_m_rt::*;

use cortex_m_rtos::os::*;
use cortex_m_rtos_tests::*;

fn idle_func() {
    exit_success();
}

#[entry]
fn main() -> ! {
    let main_token = MainToken::new();

    KERNEL.start(cp(), main_token, |_, token| {
        KERNEL.set_idle_func(idle_func, token);
    });
}
