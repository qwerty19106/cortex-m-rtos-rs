#![no_main]
#![no_std]

use cortex_m_rt::entry;

use cortex_m_rtos::common::*;
pub use cortex_m_rtos_tests::*;

static A5: Once<StackItem<u8>> = Once::new(StackItem::new(5));
static A7: Once<StackItem<u8>> = Once::new(StackItem::new(7));
static A9: Once<StackItem<u8>> = Once::new(StackItem::new(9));

#[entry]
fn main() -> ! {
    let a5 = A5.get();
    let a7 = A7.get();
    let a9 = A9.get();

    let mut stack = Stack::new();
    assert!(stack.is_empty());

    // First push
    stack.push(a5);
    stack.push(a7);
    stack.push(a9);

    // First pop (partial)
    let a9_ = stack.pop().unwrap();
    let a7_ = stack.pop().unwrap();

    assert_eq!(**a9_, 9);
    assert_eq!(**a7_, 7);
    assert!(!stack.is_empty());

    // Push and full pop
    stack.push(a9_);
    let a9__ = stack.pop().unwrap();
    let a5__ = unsafe { stack.pop_unchecked() };

    assert_eq!(**a5__, 5);
    assert_eq!(**a9__, 9);
    assert!(stack.is_empty());

    // exit by itm
    exit_success();
}
