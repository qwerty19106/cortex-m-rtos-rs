#![no_main]
#![no_std]

use cortex_m_rt::entry;

use cortex_m_rtos::common::*;
pub use cortex_m_rtos_tests::*;

static A5: Once<StackItem<u8>> = Once::new(StackItem::new(5));
static A7: Once<StackItem<u8>> = Once::new(StackItem::new(7));
static A9: Once<StackItem<u8>> = Once::new(StackItem::new(9));

#[entry]
fn main() -> ! {
    let a5 = A5.get();
    let a7 = A7.get();
    let a9 = A9.get();

    let mut queue = PriorityQueue::new();
    assert!(queue.is_empty());

    // First push
    queue.push(Priority::Normal, a5);
    queue.push(Priority::High, a7);
    queue.push(Priority::BelowNormal, a9);

    // First pop (partial)
    let a7_ = queue.pop().unwrap();
    assert_eq!(**a7_, 7);

    let a5_ = queue.pop().unwrap();
    assert_eq!(**a5_, 5);

    assert!(!queue.is_empty());
    assert!(queue.pop_by_priority(Priority::High).is_none());

    // Push and full pop
    queue.push(Priority::BelowNormal, a5_); //Priority the same as a9

    let a9__ = queue.pop_by_priority(Priority::BelowNormal).unwrap();
    assert_eq!(**a9__, 9);

    let a5__ = queue.pop().unwrap();
    assert_eq!(**a5__, 5);
    assert!(queue.is_empty());

    // exit by itm
    exit_success();
}
