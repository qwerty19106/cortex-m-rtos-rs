#![no_main]
#![no_std]

use cortex_m_rt::entry;

use cortex_m_rtos::common::*;
pub use cortex_m_rtos_tests::*;

static A: Once<Uninit<[u8; 100]>> = Once::new(Uninit::new());

#[entry]
fn main() -> ! {
    let uninit = A.get();
    let array = uninit.init([5; 100]);

    assert_eq!(array[3], 5);

    // exit by itm
    exit_success();
}
