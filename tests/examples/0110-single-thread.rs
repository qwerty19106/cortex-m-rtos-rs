#![no_main]
#![no_std]

use cortex_m::register::control;

use cortex_m_rt::*;

use cortex_m_rtos::{common::*, os::*};
use cortex_m_rtos_tests::*;

fn idle_func() {
    panic!("Unexpected idle_func!");
}

// TODO: check that THREAD in .bss (not .data)
static THREAD: Once<ThreadMemory<(), 1024>> = Once::new(ThreadMemory::new());

fn thread_func(_arg: (), _token: ThreadToken) -> ! {
    let control_val = control::read();

    // Check PSP and Unprivileged thread mode
    assert_eq!(control_val.npriv(), control::Npriv::Unprivileged);
    assert_eq!(control_val.spsel(), control::Spsel::Psp);

    exit_success();
}

#[entry]
fn main() -> ! {
    let main_token = MainToken::new();

    KERNEL.start(cp(), main_token, |_, token| {
        KERNEL.set_idle_func(idle_func, token);

        let thread = THREAD.get().to_thread(Priority::Normal, thread_func, ());
        KERNEL.add_thread(thread, token);
    });
}
