#![no_main]
#![no_std]
#![feature(asm)]

use cortex_m::{peripheral::NVIC, register::lr};
use cortex_m_rt::*;
use nb::block;

use cortex_m_rtos::{common::*, os::*};
use cortex_m_rtos_tests::*;

use cortex_m_rtos_tests::hal::{prelude::*, stm32::*, timer::*};

#[cfg(feature = "stm32f103c8")]
type TimContext = CountDownTimer<TIM2>;

#[cfg(feature = "stm32f446re")]
type TimContext = Timer<TIM2>;

static mut TIMER: Option<TimContext> = None;

#[interrupt]
fn TIM2() {
    // Check that first call of interrupt occured before jump to PSP!

    // SPSEL bit of EXC_RETURN (LR register) should be unset.
    let lr = lr::read();
    assert!(lr & 0b100 == 0, "TIM2: SPSEL check failed (LR={:?})", lr);

    // Mode bit of EXC_RETURN (LR register) should be unset.
    assert!(
        lr & 0b1000 == 0b1000,
        "TIM2: SPSEL check failed (LR={:?})",
        lr
    );

    // Clears the update flag
    let timer = unsafe { &mut TIMER };
    match timer {
        None => unreachable!(),
        Some(timer) => {
            #[cfg(feature = "stm32f103c8")]
            timer.clear_update_interrupt_flag();

            #[cfg(feature = "stm32f446re")]
            timer.clear_interrupt(Event::TimeOut);
        }
    }
}

fn idle_func() {
    unreachable!();
}

static THREAD: Once<ThreadMemory<(), 16>> = Once::new(ThreadMemory::new());

fn thread_func(_arg: (), _token: ThreadToken) -> ! {
    exit_success();
}

#[entry]
fn main() -> ! {
    let main_token = MainToken::new();

    KERNEL.start(cp(), main_token, |_, token| {
        let dp = dp();

        KERNEL.set_idle_func(idle_func, token);

        let thread = THREAD.get().to_thread(Priority::Normal, thread_func, ());
        KERNEL.add_thread(thread, token);

        #[cfg(feature = "stm32f103c8")]
        let mut timer = {
            let mut flash = dp.FLASH.constrain();
            let mut rcc = dp.RCC.constrain();

            // Freeze the configuration of all the clocks in the system and store
            // the frozen frequencies in `clocks`
            let clocks = rcc.cfgr.freeze(&mut flash.acr);

            // Configure the TIM2 timer to trigger an update with 100Hz
            #[cfg(feature = "stm32f103c8")]
            let mut timer = Timer::tim2(dp.TIM2, &clocks, &mut rcc.apb1).start_count_down(100.hz());
            timer.listen(Event::Update);
            timer
        };

        #[cfg(feature = "stm32f446re")]
        let mut timer = {
            let rcc = dp.RCC.constrain();

            // Freeze the configuration of all the clocks in the system and store
            // the frozen frequencies in `clocks`
            let clocks = rcc.cfgr.freeze();

            // Configure the TIM2 timer to trigger an update with 100Hz
            let mut timer = Timer::tim2(dp.TIM2, 100.hz(), clocks);
            timer.listen(Event::TimeOut);
            timer
        };

        // Wait for timer interrupt is pending
        block!(timer.wait()).unwrap();

        // Share timer to TIM2 interrupt
        unsafe {
            TIMER = Some(timer);
        }

        // Allow TIM2 interrupt
        // TODO: drop unsafe
        unsafe {
            NVIC::unmask(Interrupt::TIM2);
        }
    });
}
