#![no_main]
#![no_std]

use cortex_m_rt::*;

use cortex_m_rtos::{common::*, os::*};
use cortex_m_rtos_tests::*;

const VAL: u8 = 5;
const DATA_VAL: u64 = 8;
static DATA: Once<u64> = Once::new(DATA_VAL);

static THREAD: Once<ThreadMemory<(Own<u64>, u8), 128>> = Once::new(ThreadMemory::new());

fn thread_func(arg: (Own<u64>, u8), _token: ThreadToken) -> ! {
    let (data, val) = arg;
    assert_eq!(*data, DATA_VAL);
    assert_eq!(val, VAL);

    exit_success();
}

#[entry]
fn main() -> ! {
    let main_token = MainToken::new();
    KERNEL.start(
        cp(),
        main_token,
        |_, token| {
            let data = DATA.get();

            let thread = THREAD
                .get()
                .to_thread(Priority::Normal, thread_func, (data, VAL));
            KERNEL.add_thread(thread, token);
        },
    );
}
