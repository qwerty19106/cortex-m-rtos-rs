#![no_main]
#![no_std]

use core::sync::atomic::{AtomicUsize, Ordering};

use cortex_m_rt::entry;

use cortex_m_rtos::common::*;
pub use cortex_m_rtos_tests::*;

fn atomic_usize_inc_limit_test() {
    // Simple test
    let mut a = AtomicUsize::new(4);

    assert!(atomic_usize_inc_limit(&a, 5));
    assert_eq!(a.load(Ordering::SeqCst), 5);

    assert!(!atomic_usize_inc_limit(&a, 5));
    assert_eq!(a.load(Ordering::SeqCst), 5);

    assert!(atomic_usize_inc_limit(&a, 4));
    assert_eq!(a.load(Ordering::SeqCst), 6);

    // Test with usize overflow
    *a.get_mut() = usize::MAX - 1;

    assert!(atomic_usize_inc_limit(&a, 0));
    assert_eq!(a.load(Ordering::SeqCst), usize::MAX);

    assert!(!atomic_usize_inc_limit(&a, usize::MAX));
    assert_eq!(a.load(Ordering::SeqCst), usize::MAX);

    assert!(atomic_usize_inc_limit(&a, 0));
    assert_eq!(a.load(Ordering::SeqCst), 0);
}

fn atomic_usize_inc_limit_extended_test() {
    // Simple test
    let mut a = AtomicUsize::new(4);

    let (res, old) = atomic_usize_inc_limit_extended(&a, 5);
    assert!(res);
    assert_eq!(old, 4);
    assert_eq!(a.load(Ordering::SeqCst), 5);

    let (res, old) = atomic_usize_inc_limit_extended(&a, 5);
    assert!(!res);
    assert_eq!(old, 5);
    assert_eq!(a.load(Ordering::SeqCst), 5);

    let (res, old) = atomic_usize_inc_limit_extended(&a, 4);
    assert!(res);
    assert_eq!(old, 5);
    assert_eq!(a.load(Ordering::SeqCst), 6);

    // Test with usize overflow
    *a.get_mut() = usize::MAX - 1;

    let (res, old) = atomic_usize_inc_limit_extended(&a, 0);
    assert!(res);
    assert_eq!(old, usize::MAX - 1);
    assert_eq!(a.load(Ordering::SeqCst), usize::MAX);

    let (res, old) = atomic_usize_inc_limit_extended(&a, usize::MAX);
    assert!(!res);
    assert_eq!(old, usize::MAX);
    assert_eq!(a.load(Ordering::SeqCst), usize::MAX);

    let (res, old) = atomic_usize_inc_limit_extended(&a, 0);
    assert!(res);
    assert_eq!(old, usize::MAX);
    assert_eq!(a.load(Ordering::SeqCst), 0);
}

fn atomic_usize_dec_non_zero_test() {
    let mut a = AtomicUsize::new(1);

    assert!(atomic_usize_dec_non_zero(&a));
    assert_eq!(a.load(Ordering::SeqCst), 0);

    assert!(!atomic_usize_dec_non_zero(&a));
    assert_eq!(a.load(Ordering::SeqCst), 0);

    *a.get_mut() = usize::MAX;
    assert!(atomic_usize_dec_non_zero(&a));
    assert_eq!(a.load(Ordering::SeqCst), usize::MAX - 1);
}

#[entry]
fn main() -> ! {
    atomic_usize_inc_limit_test();
    atomic_usize_inc_limit_extended_test();
    atomic_usize_dec_non_zero_test();

    // exit by itm
    exit_success();
}
