#![no_main]
#![no_std]

use cortex_m_rt::entry;

use cortex_m_rtos::os::{token::*, *};

use cortex_m_rtos_tests::{hal::stm32::*, *};


#[cfg(feature = "stm32f103c8")]
static CONTEXT: ISRContext<u32, 6> = ISRContext::new();

#[cfg(feature = "stm32f446re")]
static CONTEXT: ISRContext<u32, 6> = ISRContext::new();

#[entry]
fn main() -> ! {
    const ISR_TOKEN: ISRTokenNumber = unsafe { ISRTokenNumber::new_unchecked_const(Interrupt::EXTI0 as u8) };
    const ISR_TOKEN2: ISRTokenNumber = unsafe { ISRTokenNumber::new_unchecked_const(Interrupt::EXTI1 as u8) };
    let starting_token = unsafe { KernelStartingToken::new_unchecked() };

    // non-set
    assert!(CONTEXT.get::<ISR_TOKEN>().is_none());

    // set
    CONTEXT.set(12, starting_token);

    // get_unchecked
    let a = unsafe {
        CONTEXT.get_unchecked(ISR_TOKEN.into()).unwrap()
    };
    assert_eq!(*a, 12);
    *a = 13;

    // get
    let b = CONTEXT.get::<ISR_TOKEN>().unwrap();
    assert_eq!(*b, 13);

    // panic
    CONTEXT.get::<ISR_TOKEN2>();

    // exit by itm
    exit_success();
}
