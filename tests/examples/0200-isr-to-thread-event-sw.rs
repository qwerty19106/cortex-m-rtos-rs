#![no_main]
#![no_std]

use core::sync::atomic::{AtomicBool, Ordering};

use cortex_m::peripheral::NVIC;
use cortex_m_rt::entry;

use cortex_m_rtos::{
    common::*,
    os::{token::*, *},
    primitive::isr_to_thread::sw_event::*,
};

use cortex_m_rtos_tests::{
    hal::{prelude::*, stm32::*, timer::*},
    *,
};

static THREAD: Once<ThreadMemory<usize, 2048>> = Once::new(ThreadMemory::new());

#[cfg(feature = "stm32f103c8")]
type TimContext = CountDownTimer<TIM2>;

#[cfg(feature = "stm32f446re")]
type TimContext = Timer<TIM2>;

static TIM_CONTEXT: ISRContext<TimContext, { Interrupt::TIM2 as u8 }> = ISRContext::new();

#[interrupt]
fn TIM2() {
    static mut COUNTER: u32 = 0;

    const TOKEN: ISRTokenNumber =
        unsafe { ISRTokenNumber::new_unchecked_const(Interrupt::TIM2 as u8) };
    let context = TIM_CONTEXT.get::<TOKEN>();
    let timer = context.unwrap();

    // First clear interrupt flag.
    // NOTE! Clear interrupt flag at the end of interrupt can cause repeated TIM2 handler!
    #[cfg(feature = "stm32f103c8")]
    timer.clear_update_interrupt_flag();

    #[cfg(feature = "stm32f446re")]
    timer.clear_interrupt(Event::TimeOut);

    EVENT.set(TOKEN.into()).unwrap();
    if *COUNTER == 1 {
        EVENT.set(TOKEN.into()).unwrap();
    }

    *COUNTER += 1;
}

static EVENT: SWEvent = SWEvent::new(false);

fn thread_func(_: usize, token: ThreadToken) -> ! {
    let event = EVENT.get_waiter();

    event.wait(token);
    event.wait(token);

    exit_success();
}

#[entry]
fn main() -> ! {
    let main_token = MainToken::new();

    KERNEL.start(
        cp(),
        main_token,
        |_, token| {
            let dp = dp();

            #[cfg(feature = "stm32f103c8")]
            let timer = {
                let mut flash = dp.FLASH.constrain();
                let mut rcc = dp.RCC.constrain();

                // Freeze the configuration of all the clocks in the system and store
                // the frozen frequencies in `clocks`
                let clocks = rcc.cfgr.freeze(&mut flash.acr);

                // Configure the TIM2 timer to trigger an update with 100Hz
                #[cfg(feature = "stm32f103c8")]
                let mut timer =
                    Timer::tim2(dp.TIM2, &clocks, &mut rcc.apb1).start_count_down(100.hz());
                timer.listen(Event::Update);
                timer
            };

            #[cfg(feature = "stm32f446re")]
            let timer = {
                let rcc = dp.RCC.constrain();

                // Freeze the configuration of all the clocks in the system and store
                // the frozen frequencies in `clocks`
                let clocks = rcc.cfgr.freeze();

                // Configure the TIM2 timer to trigger an update with 100Hz
                let mut timer = Timer::tim2(dp.TIM2, 100.hz(), clocks);
                timer.listen(Event::TimeOut);
                timer
            };

            // Allow TIM2 interrupt
            // TODO: drop unsafe
            unsafe {
                NVIC::unmask(Interrupt::TIM2);
            }

            // Set TIM2 context
            TIM_CONTEXT.set(timer, token);

            let thread = THREAD.get().to_thread(Priority::Normal, thread_func, 5);
            KERNEL.add_thread(thread, token);

            hooks::BEFORE_SET.set(before_set, token);
            hooks::AFTER_SET.set(after_set, token);
            hooks::BEFORE_PENDSV_PROCESS.set(before_pendsv_process, token);
            hooks::AFTER_PENDSV_PROCESS.set(after_pendsv_process, token);
            hooks::BEFORE_SVC_WAIT.set(before_svc_wait, token);
            hooks::AFTER_SVC_WAIT.set(after_svc_wait, token);
            hooks::BEFORE_WAIT.set(before_wait, token);
            hooks::AFTER_WAIT.set(after_wait, token);
        },
    );
}

fn before_set(_event: &SWEvent, flag: &AtomicBool) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [false, false, true];

    assert_eq!(check[index], flag.load(Ordering::SeqCst));

    unsafe {
        COUNTER = index + 1;
    }
}

fn after_set(_event: &SWEvent, flag: &AtomicBool, res: &Result<(), ISRQueueExhausted>) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [(true, Ok(())), (true, Ok(())), (true, Ok(()))];

    assert_eq!(check[index].0, flag.load(Ordering::SeqCst));
    assert_eq!(check[index].1, *res);

    unsafe {
        COUNTER = index + 1;
    }
}

fn before_pendsv_process(_event: &SWEvent, pending_thread: &Option<Own<Thread>>) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [true, true];

    assert_eq!(check[index], pending_thread.is_some());

    unsafe {
        COUNTER = index + 1;
    }
}

fn after_pendsv_process(_event: &SWEvent, pending_thread: &Option<Own<Thread>>) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [false, false];

    assert_eq!(check[index], pending_thread.is_some());

    unsafe {
        COUNTER = index + 1;
    }
}

fn before_svc_wait(_event: &SWEvent, flag: &AtomicBool, pending_thread: &Option<Own<Thread>>) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [(false, false), (false, false)];

    assert_eq!(check[index].0, flag.load(Ordering::SeqCst));
    assert_eq!(check[index].1, pending_thread.is_some());

    unsafe {
        COUNTER = index + 1;
    }
}

fn after_svc_wait(_event: &SWEvent, flag: &AtomicBool, pending_thread: &Option<Own<Thread>>) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [(false, true), (false, true)];

    assert_eq!(check[index].0, flag.load(Ordering::SeqCst));
    assert_eq!(check[index].1, pending_thread.is_some());

    unsafe {
        COUNTER = index + 1;
    }
}

fn before_wait(_event: &SWEvent, flag: &AtomicBool) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [false, false];

    assert_eq!(check[index], flag.load(Ordering::SeqCst));

    unsafe {
        COUNTER = index + 1;
    }
}

fn after_wait(_event: &SWEvent, flag: &AtomicBool) {
    static mut COUNTER: usize = 0;
    let index = unsafe { COUNTER };

    let check = [false, false];

    assert_eq!(check[index], flag.load(Ordering::SeqCst));

    unsafe {
        COUNTER = index + 1;
    }
}
