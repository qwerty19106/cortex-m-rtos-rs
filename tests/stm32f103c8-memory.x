MEMORY
{
  /* FLASH and RAM are mandatory memory regions */
  FLASH : ORIGIN = 0x08000000, LENGTH = 64K
  RAM : ORIGIN = 0x20000000, LENGTH = 20K
}