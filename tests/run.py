#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import subprocess
import shutil
import os
import pathlib
from time import sleep
import socket
import sys
import argparse


def run_test(name: str, target: str, features: list, should_success: bool,
             swo_socket: socket.socket) -> bool:
    success = True
    load_log = None
    run_log = None
    swo_log = None
    diff_log = None

    try:
        # Flash device
        load_cmd = ['gdb-multiarch',
                    '--nx',
                    '-batch',
                    '--return-child-result',
                    '-ex', 'target remote localhost:50000',
                    '-ex', 'monitor SWO DisableTarget 1',
                    '-ex', 'monitor reset',
                    '-ex', 'load',
                    '-ex', 'monitor SWO EnableTarget 0 0 1 0',
                    '-ex', 'disconnect',
                    '-ex', 'quit 0',
                    'target/{}/release/examples/{}'.format(target, name)]
        if args.verbose:
            print("> {}".format(" ".join(load_cmd)))
        load_result = subprocess.run(load_cmd, stdout=subprocess.PIPE,
                                     stderr=subprocess.STDOUT, encoding="utf-8")

        if load_result.returncode != 0:
            print(
                "Test {}: load failure. See logs/{}/load.gdb.log for details".format(name, name))
            load_log = load_result.stdout
            success = False
            return False

        # Debug with LLDB. Execute commands.lldb
        run_cmd = ['lldb',
                   '-x',
                   '-s', 'commands.lldb',
                   '--batch',
                   'target/{}/release/examples/{}'.format(target, name)]

        if args.verbose:
            print("> {}".format(" ".join(run_cmd)))
        run_result = subprocess.run(run_cmd, stdout=subprocess.PIPE,
                                    stderr=subprocess.STDOUT, encoding="utf-8")

        # Read SWO log
        sleep(0.1)
        try:
            swo_log_bytes = swo_socket.recv(100 * 1024)
            swo_log = swo_log_bytes.decode("utf-8")
        except BlockingIOError:
            pass

        if not run_result.returncode in (1, 2):
            print(
                "Test {}: failure. LLDB return unexpected code {}. See logs/{}/swo.log for details".format(name, run_result.returncode, name))
            run_log = run_result.stdout
            success = False
            return False

        if should_success:
            # Test should pass successfully
            if run_result.returncode != 1:
                print(
                    "Test {}: failure. See logs/{}/swo.log for details".format(name, name))
                run_log = run_result.stdout
                success = False
                return False
        else:
            # Test should fail
            if run_result.returncode != 2:
                print(
                    "Test {}: failure. Should panic, but exit successfully!".format(name))
                run_log = run_result.stdout
                success = False
                return False

            # Compare SWO log
            with open("logs/swo.log", "w") as f:
                f.write(swo_log)

            diff_result = subprocess.run(['diff', '-B', 'examples/{}.swo.log'.format(name), 'logs/swo.log'],
                                         input=swo_log,
                                         stdout=subprocess.PIPE,
                                         stderr=subprocess.STDOUT, encoding="utf-8")
            if diff_result.returncode != 0:
                diff_log = diff_result.stdout
                print(
                    "Test {}: failure. SWO logs diff is not empty! See logs/{}/swo.log.diff for details".format(name, name))
                success = False
                return False

        print("Test {}: ok".format(name))

        return True

    finally:
        if not success:
            log_dir = "logs/{}".format(name)
            os.mkdir(log_dir)

            # Save load log
            if load_log:
                with open(log_dir + "/load.gdb.log", "w") as f:
                    f.write(load_log)

            # Save GDB log
            if run_log:
                with open(log_dir + "/run.lldb.log", "w") as f:
                    f.write(run_log)

            # Save SWO log
            if swo_log:
                with open(log_dir + "/swo.log", "w") as f:
                    f.write(swo_log)

            if diff_log:
                # Save diff log
                with open(log_dir + "/swo.log.diff", "w") as f:
                    f.writelines(diff_log)


parser = argparse.ArgumentParser()
parser.add_argument('--build-only', action='store_true',
                    help='Build tests only')
parser.add_argument('--verbose', action='store_true',
                    help='Show full commands')
parser.add_argument('device', choices=['stm32f103c8', 'stm32f446re'],
                    help='One of variants: stm32f103c8, stm32f446re')
parser.add_argument('names', nargs='*',
                    help='Run one or more tests by name')
args = parser.parse_args()

# Parse device
device_to_target = {
    "stm32f103c8": "thumbv7m-none-eabi",
    "stm32f446re": "thumbv7em-none-eabihf"
}
device = args.device
target = device_to_target[device]

# Chdir
script_dir = os.path.dirname(os.path.abspath(__file__))
os.chdir(script_dir)

# Load list of package names
with open("tests.json") as f:
    tests = json.load(f)
    success = tests["success"]
    failure = tests["failure"]

tests = []
for test in success:
    test["success"] = True
    tests.append(test)

for test in failure:
    test["success"] = False
    tests.append(test)

if len(args.names) > 0:
    # Find test by name
    matches = [test for test in tests if test["name"] in args.names]
    if len(matches) == 0:
        print("No test {} found".format(args.name))
        sys.exit(1)

    tests = matches

# Clear logs
shutil.rmtree("logs", ignore_errors=True)
os.mkdir("logs")

if not args.build_only:
    # Run JLinkGDBServer
    gdb_server_cmd = ['JLinkGDBServerCLExe',
                      '-select', 'usb=000771541653',
                      '-if', 'swd',
                      '-port', '50000',
                      '-telnetport', '50001',
                      '-device', device,
                      '-strict',
                      '-log', 'logs/jlink.log']

    if args.verbose:
        print("> {}".format(" ".join(gdb_server_cmd)))
    gdb_server = subprocess.Popen(
        gdb_server_cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    # Connect to SWO output
    sleep(0.1)
    swo_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    swo_socket.connect(("127.0.0.1", 50001))
    swo_socket.setblocking(0)

try:
    # Compile examples
    for test in tests:
        name = test["name"]
        features = test["features"]

        features.insert(0, device)
        build_cmd = ['cargo',
                     '+nightly',
                     'build',
                     '--target', target,
                     '--release',
                     '--example', name,
                     '--features', ",".join(features)]

        env = os.environ.copy()
        env["RUST_BACKTRACE"] = "full"

        print("Build {}".format(name))
        if args.verbose:
            print("> {}".format(" ".join(build_cmd)))

        res = subprocess.run(build_cmd, env=env)
        res.check_returncode()

    if not args.build_only:
        # Check JLinkGDBServer
        if not gdb_server.poll() is None:
            print("JLinkGDBServer exited unexpectedly. See logs/jlink.log for details.")
            sys.exit(1)

        # Read first line of SWO log: SEGGER J-Link GDB Server Vx.xxx - Terminal output channel
        sleep(1)
        swo_socket.recv(100 * 1024)

        # Run examples
        fails_count = 0
        for test in tests:
            if not run_test(test["name"], target, test["features"], test["success"], swo_socket):
                fails_count += 1

        if fails_count == 0:
            print("All tests ok!")
        else:
            print("Test stats: {}/{} failure".format(fails_count, len(tests)))
            sys.exit(1)

finally:
    if not args.build_only:
        swo_socket.close()

        gdb_server.terminate()
        gdb_server.wait()
