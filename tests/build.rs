use std::path::PathBuf;
use std::{env, fs};

fn main() {
    let stm32f103c8 = cfg!(feature = "stm32f103c8");
    let stm32f446re = cfg!(feature = "stm32f446re");
    let devices = stm32f103c8 as u8 + stm32f446re as u8;

    if devices == 0 {
        panic!("One of features should be enabled: stm32f103c8, stm32f446re");
    }
    if devices > 1 {
        panic!("Only one of features should be enabled: stm32f103c8, stm32f446re");
    }

    // Add device memory.x to output directory
    #[cfg(feature = "stm32f103c8")]
    let device_name = "stm32f103c8";

    #[cfg(feature = "stm32f446re")]
    let device_name = "stm32f446re";

    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());

    fs::copy(
        format!("{}-memory.x", device_name),
        out_dir.join("memory.x"),
    )
    .unwrap();

    // Add output directory to search path
    println!("cargo:rustc-link-search={}", out_dir.display());
}
