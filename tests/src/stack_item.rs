use core::ops::{Deref, DerefMut};
use core::ptr::NonNull;

use cortex_m_rtos::common::*;

pub struct StackItem<T> {
    data: T,
    next: Option<NonNull<Self>>,
}

impl<T> NextField for StackItem<T> {
    fn next(&mut self) -> &mut Option<NonNull<Self>> {
        &mut self.next
    }
}

impl<T> StackItem<T> {
    pub const fn new(data: T) -> Self {
        Self { data, next: None }
    }
}

impl<T> Deref for StackItem<T> {
    type Target = T;

    #[inline]
    fn deref(&self) -> &T {
        &self.data
    }
}

impl<T> DerefMut for StackItem<T> {
    #[inline]
    fn deref_mut(&mut self) -> &mut T {
        &mut self.data
    }
}
