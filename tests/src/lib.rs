#![feature(const_panic)]
#![feature(panic_info_message)]
#![no_std]

use core::{
    panic::PanicInfo,
    sync::atomic::{AtomicUsize, Ordering},
};

use cortex_m::{iprint, iprintln, peripheral::ITM};
use cortex_m_rt::*;

#[cfg(feature = "stm32f103c8")]
pub use stm32f1xx_hal as hal;

#[cfg(feature = "stm32f446re")]
pub use stm32f4xx_hal as hal;

static TMP: AtomicUsize = AtomicUsize::new(0);

#[inline(never)]
#[no_mangle]
pub extern "C" fn exit_success() -> ! {
    loop {
        // Add some side effect to prevent this from turning into a UDF instruction
        // see rust-lang/rust#28728 for details

        // Also it prevent optimize exit_success and panic_infinity_loop functions to one function
        TMP.fetch_add(1, Ordering::SeqCst);
    }
}

#[inline(never)]
#[no_mangle]
extern "C" fn panic_infinity_loop() -> ! {
    loop {
        // add some side effect to prevent this from turning into a UDF instruction
        // see rust-lang/rust#28728 for details

        // Also it prevent optimize exit_success and panic_infinity_loop functions to one function
        TMP.fetch_add(2, Ordering::SeqCst);
    }
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    cortex_m::interrupt::disable();

    let itm = unsafe { &mut *ITM::ptr() };
    let stim = &mut itm.stim[0];

    // Copy of implementation of fmt::Display for PanicInfo
    if let Some(message) = info.message() {
        iprint!(stim, "panicked at '{}', ", message);
    } else if let Some(payload) = info.payload().downcast_ref::<&'static str>() {
        iprint!(stim, "panicked at '{}', ", payload);
    }

    // NOTE: we cannot use downcast_ref::<String>() here
    // since String is not available in libcore!
    // The payload is a String when `std::panic!` is called with multiple arguments,
    // but in that case the message is also available.

    let location = info.location().unwrap();
    let mut file = location.file();

    // Get relative path to cortex-m-rtos
    if let Some(index) = file.rfind("cortex-m-rtos") {
        file = &file[index..];
    }
    iprintln!(stim, "{}:{}:{}", file, location.line(), location.column());

    panic_infinity_loop()
}

#[exception]
fn HardFault(_ef: &ExceptionFrame) -> ! {
    panic!("HardFault");
}

#[exception]
fn BusFault() -> ! {
    panic!("BusFault");
}

#[exception]
fn MemoryManagement() -> ! {
    panic!("MemoryManagement");
}

#[exception]
fn UsageFault() -> ! {
    panic!("UsageFault");
}

#[exception]
fn DefaultHandler(irqn: i16) {
    panic!("DefaultHandler: irqn={:?}", irqn);
}

/// Get access to the core peripherals from the cortex-m crate
pub fn cp() -> cortex_m::Peripherals {
    cortex_m::Peripherals::take().unwrap()
}

/// Get access to the device specific peripherals from the peripheral access crate
pub fn dp() -> hal::stm32::Peripherals {
    hal::stm32::Peripherals::take().unwrap()
}

mod stack_item;
pub use stack_item::StackItem;

/// Enable and start CYCCNT
pub fn enable_cyccnt(mut cp: cortex_m::Peripherals) {
    cp.DCB.enable_trace();
    cp.DWT.enable_cycle_counter();
}

#[inline]
pub fn u32_to_hex(val: u32, res: &mut [u8; 8]) {
    for i in 0..8 {
        let b = ((val >> (4 * i)) & 0x0F) as u8;

        if b < 10 {
            res[7 - i] = b + b'0';
        } else {
            res[7 - i] = b + b'A' - 10;
        }
    }
}

pub fn print(s: &str) {
    let itm = unsafe { &mut *ITM::ptr() };
    let stim = &mut itm.stim[0];

    iprint!(stim, s);
    iprint!(stim, "\n");
}
