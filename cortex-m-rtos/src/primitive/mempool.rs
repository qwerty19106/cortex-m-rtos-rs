//use core::num::NonZeroUsize;
use core::ops::{Deref, DerefMut};
use core::ptr::drop_in_place;

use generic_array::{ArrayLength, GenericArray};

use crate::common::*;
//use evr;
//use rtx::core_cm::exclusive_access::*;
//use ::rtx::core_cm::EXCLUSIVE_ACCESS;
//use rtx::memory::RtxMemory;
//use rtx::rtx_os::OsRtxThread;

//#include "rtx_lib.h"
//
//
////  OS Runtime Object Memory Usage
//#if ((defined(OS_OBJ_MEM_USAGE) && (OS_OBJ_MEM_USAGE != 0)))
//osRtxObjectMemUsage_t osRtxMemoryPoolMemUsage \
//__attribute__((section(".data.os.mempool.obj"))) =
//{ 0U, 0U, 0U };
//#endif

//  ==== RtxMemoryBlock ====

/// Container of data and pointer to pool which allocated this block.
pub struct MemoryBlockInner<T> {
    /// Pointer to pool, which is owner of this block.
    #[allow(dead_code)]
    pool: *mut OsRtxMpInfo<T>,

    /// Data of block
    data: T,
}

/// Container for static allocation of memory pool data.
///
/// It is aligned on 4 (or more) bytes, because **StackItem** aligned on 4 bytes.
/// See #[repr(Rust)] for more details (https://doc.rust-lang.org/beta/nomicon/repr-rust.html).
///
/// See usage example for OsRtxMpInfo documentation.
pub type MemoryBlockContainer<T> = StackItem<MemoryBlockInner<T>>;

/// A pointer type for memory pool allocation.
///
/// MemoryBlock<T> has owning semantic at well as Own<T>.
/// However we must implement drop for it, which will be return MemoryBlockContainer<T> to its memory pool.
/// Thus we need custom type (not Own<T>) because it has other semantic.
#[repr(transparent)]
pub struct MemoryBlock<T> {
    ptr: *mut MemoryBlockContainer<T>,
}

/*impl<T> MemoryBlock<T> {
    #[inline]
    pub fn as_ptr(self) -> &'static mut MemoryBlockContainer<T> {
        // We use mem::forget to prevent call of drop.
        // It is inline because mem::forget translate to 0 processor instruction, and drop not will be called.
        let ref_ = self.ptr as *mut MemoryBlockContainer<T>;
        mem::forget(self);
        unsafe { &mut *ref_ }
    }
}*/

impl<T> Deref for MemoryBlock<T> {
    type Target = T;

    #[inline]
    fn deref(&self) -> &T {
        unsafe { &(*self.ptr).data }
    }
}

impl<T> DerefMut for MemoryBlock<T> {
    #[inline]
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut (*self.ptr).data }
    }
}

impl<T> Drop for MemoryBlock<T> {
    fn drop(&mut self) {
        //Call destructor of T (if exists)
        let ptr: &mut T = self.deref_mut();
        unsafe {
            drop_in_place(ptr);
        }

        //Return block to pool
        //let (data, pool) = (self.data, self.pool);
        //pool.free(data);
        unimplemented!();
    }
}

//  ==== Memory Pool definitions ====

//TODO: переписать после реализации const generics
//https://github.com/rust-lang/rust/issues/44580
//https://github.com/rust-lang/rfcs/blob/master/text/2000-const-generics.md

/// Memory Pool Information
///
/// **Note.** Not contains `used_blocks` field!
/// The methods `osMemoryPoolGetCount` and `osMemoryPoolGetSpace` are not necessary, because its used to check pool before
/// free. But in Rust the Memory Pool must be wrapped by `Arc`, and there is no need to drop the Memory Pool manually.
/// Also to allocate and free block there is no need `used_blocks` field.
/// Thus we rely on `Arc` counter and not use `used_blocks` field.
///
/// OsRtxMpInfo not contains generic param N (blocks count in pool) and field mem[T; N] (array of blocks).
/// It would require to add N in MemoryBlock (to implement Drop). But MemoryBlock can be send to many other functions,
/// which are not related to N.
/// Instead of this OsRtxMpInfo implement StaticInit<[T; N]> to initialize self.
// TODO: дописать подробное объяснение в OsRtxMemoryPool, почему нет методов `osMemoryPoolGetCount` and `osMemoryPoolGetSpace`
pub struct OsRtxMpInfo<T> {
    /// Stack (LIFO) of free blocks
    free_blocks: ConcurrentStack<MemoryBlockInner<T>>,
}

unsafe impl<T: 'static> Sync for OsRtxMpInfo<T> {}

//TODO: переписать после реализации const generics
impl<T: InitNoArg> OsRtxMpInfo<T> {
    /// Initialize Memory Pool. Link all free blocks from mem to stack (LIFO).
    ///
    /// **mem** - statically allocated [MemoryBlockContainer<T>; N].
    ///
    /// The **mem** is aligned on 4 bytes (see MemoryBlockContainer<T> description).
    /// It is required to one can get valid (aligned) pointer of any array element.
    ///
    /// This code called only on RTX static initialization.
    ///
    /// Usage example:
    /// ```
    /// use crate::common::*;
    ///
    /// static POOL_DATA: Once<GenericArray<MemoryBlockContainer<[u32; 5]>, U20>> = Once::new();
    /// static POOL: OsRtxMpInfo<[u32; 5]> = OsRtxMpInfo::empty();
    ///
    /// fn main() {
    ///     let pool_data = POOL_DATA.get.unwrap();
    ///     POOL.init(pool_data);
    /// }
    /// ```
    fn _init<N>(
        &mut self,
        arr: Uninitialized<GenericArray<MemoryBlockContainer<T>, N>>,
        token: &cortex_m::interrupt::CriticalSection,
    ) where
        N: ArrayLength<MemoryBlockContainer<T>>,
    {
        let arr = unsafe { &mut *arr.as_ptr() };

        for block in arr {
            // Init block
            block.pool = self;
            unsafe {
                block.data.init(token);
            }

            // Put block into stack
            let block_cp = unsafe { Box::new_unchecked(block) };
            self.free_blocks.push(block_cp);
        }
    }
}

impl<T: 'static> OsRtxMpInfo<T> {
    /// Allocate a memory block from a Memory Pool.
    /// \param[in]  mp_info         memory pool info.
    /// \return address of the allocated memory block or NULL in case of no memory is available.
    pub fn alloc(&'static mut self) -> Option<MemoryBlock<T>> {
        let ret = match self.free_blocks.pop() {
            Some(box_) => Some(MemoryBlock { ptr: box_.as_ptr() }),
            None => None,
        };

        //evr::memory::block_alloc(self, &ret);
        ret
    }

    /// Return an allocated memory block back to a Memory Pool.
    /// \param[in]  mp_info         memory pool info.
    /// \param[in]  block           address of the allocated memory block to be returned to the memory pool.
    ///
    /// The caller must ensure that block allocated by self OsRtxMpInfo instance!
    fn _free(&mut self, block: &mut MemoryBlock<T>) {
        let box_ = unsafe { Box::new_unchecked(block.ptr) };
        self.free_blocks.push(box_);
        //evr::memory::block_free(self, &block);
    }
}

// /// Memory Pool Control Block
// pub struct OsRtxMemoryPool<T, A>
// where
//     A: FixedSizeArray<DataOrPointer<T>>,
// {
//     /// Object Identifier
//     id: u8,
//     /// Object State
//     state: u8,
//     /// Object Flags
//     flags: u8,
//     _reserved: u8,
//     /// Object Name
//     //const char                    *name;
//     /// Waiting Threads List
//     thread_list: *mut OsRtxThread,
//     /// Memory Pool Info
//     mp_info: OsRtxMpInfo<T, A>,
// }

// /// Attributes structure for memory pool.
// pub struct OsMemoryPoolAttr<T, A>
// where
//     A: FixedSizeArray<DataOrPointer<T>>,
// {
//     /// name of the memory pool
//     //const char                   *name;
//     /// attribute bits
//     attr_bits: u32,
//     /// memory for control block
//     cb_mem: Option<RtxMemory<OsRtxMemoryPool<T, A>>>,
//     /// memory for data storage
//     mp_mem: Option<RtxMemory<A>>,
// }

// impl<T, A> OsRtxMemoryPool<T, A>
// where
//     A: FixedSizeArray<DataOrPointer<T>>,
// {
//     //  ==== Post ISR processing ====

//     /// Memory Pool post ISR processing.
//     /// \param[in]  mp              memory pool object.
//     fn post_process(&mut self) {
//         // Check if Thread is waiting to allocate memory
//         if !self.thread_list.is_null() {
//             // Allocate memory
//             match self.mp_info.alloc() {
//                 None => {}
//                 Some(block) => {
//                     unimplemented!();
//                     // // Wakeup waiting Thread with highest Priority
//                     // thread = osRtxThreadListGet(osRtxObject(mp));
//                     // //lint -e{923} "cast from pointer to unsigned int"
//                     // osRtxThreadWaitExit(thread, (uint32_t)block, FALSE);
//                     // EvrRtxMemoryPoolAllocated(mp, block);
//                 }
//             };
//         }
//     }

//     //  ==== Service Calls ====

//     /// Create and Initialize a Memory Pool object.
//     /// \note API identical to osMemoryPoolNew
//     fn svc_new(block_count: NonZeroUsize, attr: Option<OsMemoryPoolAttr<T, A>>) /* -> Self */
//     {
//         //TODO: нужна ли эта строка? После перехода на Const Generics по-видимому можно убрать
//         //if ((__CLZ(b_count) + __CLZ(b_size)) < 32U) {

//         //let mut name
//         let mut mp: Option<RtxMemory<OsRtxMemoryPool<T, A>>>;
//         let mut mp_mem: Option<RtxMemory<A>>;

//         let b_size = size_of::<DataOrPointer<T>>();
//         let size = block_count.get() * b_size;

//         // Process attributes
//         match attr {
//             Some(attr) => {
//                 //name    = attr->name;
//                 mp = attr.cb_mem;
//                 mp_mem = attr.mp_mem;
//             }
//             None => {
//                 //name    = NULL;
//                 mp = None;
//                 mp_mem = None;
//             }
//         };

//         // Allocate object memory if not provided
//         if mp.is_none() {
//             // if (osRtxInfo.mpi.memory_pool != NULL) {
//             //     //lint -e{9079} "conversion from pointer to void to pointer to other type" [MISRA Note 5]
//             //     mp = osRtxMemoryPoolAlloc(osRtxInfo.mpi.memory_pool);
//             // } else {
//             //     //lint -e{9079} "conversion from pointer to void to pointer to other type" [MISRA Note 5]
//             //     mp = osRtxMemoryAlloc(osRtxInfo.mem.common, sizeof(os_memory_pool_t), 1U);
//             // }
//         }
//     }
// }
