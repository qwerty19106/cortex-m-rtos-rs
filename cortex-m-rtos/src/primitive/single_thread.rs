use core::cell::UnsafeCell;

use crate::{common::*, os::*};

/// Helper struct to ensure single-thread semantic
///
/// This is used to create single-thread synchronization primitives, such as SASemaphore et al.
pub struct SingleThreadHelper {
    /// Pending thread
    pending_thread: UnsafeCell<Option<Own<Thread>>>,

    /// Guard which ensure only one waiter
    // TODO: use https://github.com/tkaitchuck/constrandom and <const NUMBER: usize> in methods instead it
    pub guard: OnceGuard,
}

impl SingleThreadHelper {
    pub const fn new() -> Self {
        Self {
            pending_thread: UnsafeCell::new(None),
            guard: OnceGuard::new(),
        }
    }

    /// Get mutable reference of `pending_thread` field from Ring0 mode
    #[inline]
    pub fn pending_thread_mut(&'static self, _token: Ring0Token) -> &mut Option<Own<Thread>> {
        // SAFETY: interior mutability is allowed (ensures by Ring0Token)
        unsafe { &mut *self.pending_thread.get() }
    }

    /// Suspend current thread and store into self
    /// 
    /// Thread will be stored into `pending_thread` field
    /// 
    /// # Safety
    /// It should be called by single-thread mode.
    #[inline]
    pub unsafe fn suspend_current_thread(&'static self, token: SVCallToken) {
        let pending_thread = self.pending_thread_mut(token.into());

        // Preempt current thread
        let curr = KERNEL.svc_suspend_current(token);
        *pending_thread = Some(curr);
    }

    /// Try to resume pending thread
    /// 
    /// Take pending thread from self and resume it.
    /// If no pending thread then do nothing.
    #[inline]
    pub fn try_resume_pending_thread(&'static self, token: PendSVToken) {
        let pending_thread = self.pending_thread_mut(token.into());

        // Try wakeup waiting thread
        // SAFETY: API ensure that self.counter was increased previously by the release method
        match pending_thread.take() {
            None => {} // TODO: check optimization that write not occurs
            Some(thread) => {
                KERNEL.ring0_dispatch_thread(thread, token.into());
            }
        }
    }
}
