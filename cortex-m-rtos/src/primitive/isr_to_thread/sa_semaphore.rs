use core::{
    marker::PhantomData,
    sync::atomic::{AtomicUsize, Ordering},
};

use crate::{
    common::*,
    os::{svcall::*, *},
    primitive::SingleThreadHelper,
};

#[derive(Debug, PartialEq)]
pub enum ReleaseError {
    MaxReached = 1,
    ISRQueueExhausted = 2,
}

#[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
pub struct Hooks {
    /// The release hook (before)
    ///
    /// Arguments:
    /// - counter
    pub before_release: Hook1<AtomicUsize>,

    /// The release hook (after)
    ///
    /// Arguments:
    /// - counter
    /// - result
    pub after_release: Hook2<AtomicUsize, Result<(), ReleaseError>>,

    /// The pendsv_process hook (before)
    ///
    /// Arguments:
    /// - pending_thread
    pub before_pendsv_process: Hook1<Option<Own<Thread>>>,

    /// The pendsv_process hook (after)
    ///
    /// Arguments:
    /// - pending_thread
    pub after_pendsv_process: Hook1<Option<Own<Thread>>>,

    /// The svc_acquire hook (before)
    ///
    /// Arguments:
    /// - counter
    /// - pending_thread
    pub before_svc_acquire: Hook2<AtomicUsize, Option<Own<Thread>>>,

    /// The svc_acquire hook (after)
    ///
    /// Arguments:
    /// - counter
    /// - pending_thread
    pub after_svc_acquire: Hook2<AtomicUsize, Option<Own<Thread>>>,

    /// The acquire hook (before)
    ///
    /// Arguments:
    /// - counter
    pub before_acquire: Hook1<AtomicUsize>,
    /// The acquire hook (after)
    ///
    /// Arguments:
    /// - counter
    pub after_acquire: Hook1<AtomicUsize>,
}

#[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
impl Hooks {
    const fn new() -> Self {
        Self {
            before_release: Hook1::new(),
            after_release: Hook2::new(),
            before_pendsv_process: Hook1::new(),
            after_pendsv_process: Hook1::new(),
            before_svc_acquire: Hook2::new(),
            after_svc_acquire: Hook2::new(),
            before_acquire: Hook1::new(),
            after_acquire: Hook1::new(),
        }
    }
}

pub struct MultiRelease;
pub struct SingleRelease<const IRQN: u8>;

/// Single acquire semaphore
///
/// Acquire from Thread mode, release from ISR mode.
pub struct SASemaphore<T, const MAX: usize> {
    /// Counter
    counter: AtomicUsize,
    /// Helper for single-thread mode
    helper: SingleThreadHelper,

    _phantom: PhantomData<T>,

    #[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
    pub hooks: Hooks,
}

unsafe impl<const MAX: usize> Sync for SASemaphore<MultiRelease, MAX> {}
unsafe impl<const IRQN: u8, const MAX: usize> Sync for SASemaphore<SingleRelease<IRQN>, MAX> {}

impl<T, const MAX: usize> SASemaphore<T, MAX> {
    pub const fn new(initial: usize) -> Self {
        assert!(initial <= MAX);
        Self {
            counter: AtomicUsize::new(initial),
            helper: SingleThreadHelper::new(),
            _phantom: PhantomData,

            #[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
            hooks: Hooks::new(),
        }
    }

    extern "C" fn pendsv_process(&'static self) {
        let token = unsafe { PendSVToken::new_unchecked() };

        #[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
        self.hooks
            .before_pendsv_process
            .call(self.helper.pending_thread_mut(token.into()));

        // SAFETY: we not check self.counter because SAMRSemaphore API ensure that self.counter was increased
        // previously by the release method
        self.helper.try_resume_pending_thread(token);

        #[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
        self.hooks
            .after_pendsv_process
            .call(self.helper.pending_thread_mut(token.into()));
    }

    extern "C" fn svc_acquire(&'static self) {
        let token = SVCallToken::new();

        #[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
        self.hooks
            .before_svc_acquire
            .call(&self.counter, self.helper.pending_thread_mut(token.into()));

        // This sequence of events can occur:
        // 1) Thread mode: load self.counter return 0
        // 2) ISR mode: increment self.counter
        // 3) PendSV mode: do nothing
        // 4) SVCall mode: preempt this thread
        //
        // Also this sequence of events can lead to unnecessary context switching:
        // 1) Thread mode: load self.counter return 0
        // 2) Thread mode: call SVCall
        // 3) ISR mode: increment self.counter
        // 4) SVCall mode: preempt current thread
        // 5) PendSV mode: resume this thread
        //
        // To prevent it we should check self.counter in SVCall mode:
        // 4) SVCall mode: check that self.counter==0 again
        // 5) SVCall mode: if false then do nothing
        // 6) SVCall mode: else preempt this thread
        //
        // See crate::primitive description for details.
        if self.counter.load(Ordering::Relaxed) != 0 {
            return;
        }

        // Suspend current thread
        // SAFETY: SASemaphore API ensure single-thread access
        unsafe {
            self.helper.suspend_current_thread(token);
        }

        #[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
        self.hooks
            .after_svc_acquire
            .call(&self.counter, self.helper.pending_thread_mut(token.into()));
    }

    fn acquire(&'static self, token: ThreadToken) {
        #[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
        self.hooks.before_acquire.call(&self.counter);

        // Check counter
        if self.counter.load(Ordering::Relaxed) == 0 {
            // Call SVCall to preempt current thread
            svc1n_static(Self::svc_acquire, self, token);
        }

        // Decrement counter

        // SAFETY: API ensure that self.counter != 0 because
        // 1) ISR can only increment self.counter
        // 2) self.counter != 0 after SVCall mode (see svc_acquire and pendsv_process)
        atomic_usize_dec_non_zero(&self.counter);

        #[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
        self.hooks.after_acquire.call(&self.counter);
    }
}

impl<const MAX: usize> SASemaphore<MultiRelease, MAX> {
    #[inline]
    pub fn get_acquire(&'static self) -> SAMRSemaphoreAcquire<MAX> {
        self.helper.guard.call();
        SAMRSemaphoreAcquire(self)
    }

    pub fn release(&'static self, token: ISRToken) -> Result<(), ReleaseError> {
        #[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
        self.hooks.before_release.call(&self.counter);

        let res = {
            // Try increase counter
            if !atomic_usize_inc_limit(&self.counter, MAX) {
                Err(ReleaseError::MaxReached)
            } else {
                // Try add task to ISRQueue
                if !ISR_QUEUE.push(ISRTask::new(self, Self::pendsv_process), token) {
                    Err(ReleaseError::ISRQueueExhausted)
                } else {
                    KERNEL.isr_request_pendsv(token.into());
                    Ok(())
                }
            }
        };

        #[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
        self.hooks.after_release.call(&self.counter, &res);

        res
    }
}

impl<const IRQN: u8, const MAX: usize> SASemaphore<SingleRelease<IRQN>, MAX> {
    #[inline]
    pub fn get_acquire(&'static self) -> SASRSemaphoreAcquire<IRQN, MAX> {
        self.helper.guard.call();
        SASRSemaphoreAcquire(self)
    }

    // TODO: add <const token: ISRTokenNumber>
    // add const _: () = assert!(token.irqn() == IRQN);
    // Blocked on ICE https://github.com/rust-lang/rust/issues/70586
    pub fn release(
        &'static self,
        token: ISRTokenNumber,
    ) -> Result<(), ReleaseError> {
        // Check irqn
        assert!(token.irqn() == IRQN);

        #[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
        self.hooks.before_release.call(&self.counter);

        let res = {
            // Try increase counter
            // SAFETY: Optimization! Single-release access not requires CAS!
            let counter = self.counter.load(Ordering::Relaxed);
            if counter == MAX {
                Err(ReleaseError::MaxReached)
            } else {
                self.counter.store(counter + 1, Ordering::Relaxed);

                // Try add task to ISRQueue
                if !ISR_QUEUE.push(ISRTask::new(self, Self::pendsv_process), token.into()) {
                    Err(ReleaseError::ISRQueueExhausted)
                } else {
                    KERNEL.isr_request_pendsv(token.into());
                    Ok(())
                }
            }
        };

        #[cfg(feature = "isr-to-thread-sa-semaphore-hooks")]
        self.hooks.after_release.call(&self.counter, &res);

        res
    }
}

#[repr(transparent)]
pub struct SAMRSemaphoreAcquire<const MAX: usize>(&'static SASemaphore<MultiRelease, MAX>);

#[repr(transparent)]
pub struct SASRSemaphoreAcquire<const IRQN: u8, const MAX: usize>(
    &'static SASemaphore<SingleRelease<IRQN>, MAX>,
);

unsafe impl<const MAX: usize> Send for SAMRSemaphoreAcquire<MAX> {}
unsafe impl<const IRQN: u8, const MAX: usize> Send for SASRSemaphoreAcquire<IRQN, MAX> {}

impl<const MAX: usize> !Clone for SAMRSemaphoreAcquire<MAX> {}
impl<const IRQN: u8, const MAX: usize> !Clone for SASRSemaphoreAcquire<IRQN, MAX> {}

impl<const MAX: usize> SAMRSemaphoreAcquire<MAX> {
    #[inline]
    pub fn acquire(&self, token: ThreadToken) {
        self.0.acquire(token);
    }
}

impl<const IRQN: u8, const MAX: usize> SASRSemaphoreAcquire<IRQN, MAX> {
    #[inline]
    pub fn acquire(&self, token: ThreadToken) {
        self.0.acquire(token);
    }
}
