use core::sync::atomic::{AtomicBool, Ordering};

use crate::{
    os::{svcall::*, *},
    primitive::SingleThreadHelper,
};

#[cfg(feature = "isr-to-thread-sw-event-hooks")]
use crate::common::*;

#[cfg(feature = "isr-to-thread-sw-event-hooks")]
use hooks::*;

#[derive(Debug, PartialEq)]
pub struct ISRQueueExhausted;

/// Event with single-waiting semantic
pub struct SWEvent {
    /// Internal flag
    flag: AtomicBool,
    /// Helper for single-thread mode
    helper: SingleThreadHelper,
}

unsafe impl Sync for SWEvent {}

impl SWEvent {
    pub const fn new(initial: bool) -> Self {
        Self {
            flag: AtomicBool::new(initial),
            helper: SingleThreadHelper::new(),
        }
    }

    /// Get waiter.
    ///
    /// Note! Move result of it into thread to call `wait`.
    #[inline]
    pub fn get_waiter(&'static self) -> SWEventWaiter {
        self.helper.guard.call();
        SWEventWaiter(self)
    }

    /// Wakes up the pending thread. If there is no pending thread, sets the internal flag to true.
    ///
    /// Note! It do nothing if internal flag is true already.
    /// [Semaphores](super::semaphore_sa) gives more gaurantees.
    pub fn set(&'static self, token: ISRToken) -> Result<(), ISRQueueExhausted> {
        #[cfg(feature = "isr-to-thread-sw-event-hooks")]
        BEFORE_SET.call(self, &self.flag);

        let res = {
            if self.flag.load(Ordering::Relaxed) {
                Ok(())
            } else {
                self.flag.store(true, Ordering::Relaxed);

                // Try add task to ISRQueue
                if !ISR_QUEUE.push(ISRTask::new(self, Self::pendsv_process), token) {
                    Err(ISRQueueExhausted)
                } else {
                    KERNEL.isr_request_pendsv(token);
                    Ok(())
                }
            }
        };

        #[cfg(feature = "isr-to-thread-sw-event-hooks")]
        AFTER_SET.call(self, &self.flag, &res);

        res
    }

    extern "C" fn pendsv_process(&'static self) {
        let token = unsafe { PendSVToken::new_unchecked() };

        #[cfg(feature = "isr-to-thread-sw-event-hooks")]
        BEFORE_PENDSV_PROCESS.call(self, self.helper.pending_thread_mut(token.into()));

        // SAFETY: we do not check self.flag because API ensure that self.flag was set previously by the set method
        self.helper.try_resume_pending_thread(token);

        #[cfg(feature = "isr-to-thread-sw-event-hooks")]
        AFTER_PENDSV_PROCESS.call(self, self.helper.pending_thread_mut(token.into()));
    }

    extern "C" fn svc_wait(&'static self) {
        let token = SVCallToken::new();

        #[cfg(feature = "isr-to-thread-sw-event-hooks")]
        BEFORE_SVC_WAIT.call(
            self,
            &self.flag,
            self.helper.pending_thread_mut(token.into()),
        );

        // Check self.flag again
        // It prevent data race which cause error or unnecessary context switching.
        // See crate::primitive and crate::primitive::isr_to_thread::sa_semaphore::svc_acquire for details.
        if self.flag.load(Ordering::Relaxed) {
            return;
        }

        // Suspend current thread
        // SAFETY: SWEvent API ensure single-thread access
        unsafe {
            self.helper.suspend_current_thread(token);
        }

        #[cfg(feature = "isr-to-thread-sw-event-hooks")]
        AFTER_SVC_WAIT.call(
            self,
            &self.flag,
            self.helper.pending_thread_mut(token.into()),
        );
    }
}

#[repr(transparent)]
pub struct SWEventWaiter(&'static SWEvent);

unsafe impl Send for SWEventWaiter {}

impl !Clone for SWEventWaiter {}

impl SWEventWaiter {
    /// Clear internal flag if it is true.
    /// Otherwise, block until `set` will be called.
    pub fn wait(&self, token: ThreadToken) {
        #[cfg(feature = "isr-to-thread-sw-event-hooks")]
        BEFORE_WAIT.call(self.0, &self.0.flag);

        // Check flag
        if !self.0.flag.load(Ordering::Relaxed) {
            // Call SVCall to preempt current thread
            svc1n_static(SWEvent::svc_wait, &self.0, token);
        }

        // Reset flag

        // SAFETY: API ensure that self.flag == true because
        // 1) ISR can only set self.flag
        // 2) self.flag == true after SVCall mode (see svc_acquire and pendsv_process)
        self.0.flag.store(false, Ordering::Relaxed);

        #[cfg(feature = "isr-to-thread-sw-event-hooks")]
        AFTER_WAIT.call(self.0, &self.0.flag);
    }

    /// Reset the internal flag to false.
    #[inline]
    pub fn clear(&'static self, _token: ThreadToken) {
        self.0.flag.store(false, Ordering::Relaxed);
    }
}

#[cfg(feature = "isr-to-thread-sw-event-hooks")]
pub mod hooks {
    use core::sync::atomic::AtomicBool;

    use super::*;

    /// SWEvent set event (before)
    ///
    /// Arguments:
    /// - event
    /// - flag
    pub static BEFORE_SET: Hook2<SWEvent, AtomicBool> = Hook2::new();

    /// SWEvent set event (after)
    ///
    /// Arguments:
    /// - event
    /// - flag
    /// - result
    pub static AFTER_SET: Hook3<SWEvent, AtomicBool, Result<(), ISRQueueExhausted>> = Hook3::new();

    /// SWEvent pendsv_process event (before)
    ///
    /// Arguments:
    /// - event
    /// - pending_thread
    pub static BEFORE_PENDSV_PROCESS: Hook2<SWEvent, Option<Own<Thread>>> = Hook2::new();

    /// SWEvent pendsv_process event (after)
    ///
    /// Arguments:
    /// - event
    /// - pending_thread
    pub static AFTER_PENDSV_PROCESS: Hook2<SWEvent, Option<Own<Thread>>> = Hook2::new();

    /// The svc_wait hook (before)
    ///
    /// Arguments:
    /// - event
    /// - flag
    /// - pending_thread
    pub static BEFORE_SVC_WAIT: Hook3<SWEvent, AtomicBool, Option<Own<Thread>>> = Hook3::new();

    /// The svc_wait hook (after)
    ///
    /// Arguments:
    /// - event
    /// - flag
    /// - pending_thread
    pub static AFTER_SVC_WAIT: Hook3<SWEvent, AtomicBool, Option<Own<Thread>>> = Hook3::new();

    /// SWEvent wait event (before)
    ///
    /// Arguments:
    /// - event
    /// - flag
    pub static BEFORE_WAIT: Hook2<SWEvent, AtomicBool> = Hook2::new();

    /// SWEvent wait event (after)
    ///
    /// Arguments:
    /// - event
    /// - flag
    pub static AFTER_WAIT: Hook2<SWEvent, AtomicBool> = Hook2::new();
}
