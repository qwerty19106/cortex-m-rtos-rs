//! Synchronization primitives
//!
//! # How to create primitive
//! - Add as strict restrictions as possible to the API. This will allow you to apply more optimizations.
//! For example, a semaphore can be single acquire and single release. Single acquire and multy release variant will be
//! a bit slower.
//! - IMPORTANT! Perform any operations with thread only from SVCall and PendSV mode. SVCall and PendSV have the same
//! priority, thus synchronization is not required. Kernel API ensure it by SVCallToken, PendSVToken and Ring0Token.
//! - Change atomics only from ISR mode and Thread mode. Do not change atomics atomics from SVCall and PendSV mode!
//! It is optimization to prevent unnecessary calls SVCall. Also it simplify implementation of lock-free algorithms.
//! - Create API that ISR mode can only decrease or only increase atomics. It allows to skip some checks in SVCall and
//! PendSV mode.
//! - ISR can be called between load atomic in Thread mode and call SVCall. Thus needs to perform check atomic value 
//! (only load, no store) in SVCall mode. It add 2 ticks as runtime overhead.
//! 
//! ## Example of isr_to_thread synchronization primitive
//! See [crate::primitive::isr_to_thread::semaphore_sa] source code as example.
//! 
//! The code in Thread mode may be like to (pseudo code):
//! ```
//! check atomic
//! if false then call SVCall
//! else change atomic (for example, CAS within loop)
//! ```
//! 
//! The code in SVCall mode may be like to (pseudo code):
//! ```
//! check atomic
//! if true then do nothing
//! else preempt current thread
//! ```
//! 
//! The code in PendSV mode may be like to (pseudo code):
//! ```
//! check that exists waiting thread
//! if true then resume waiting thread
//! else do nothing
//! ```
//! 
//! The code in ISR mode may be like to (pseudo code):
//! ```
//! change atomic (for example, CAS within loop)
//! try add task to ISRQueue
//! call PendSV
//! ```

pub mod isr_to_thread;

mod single_thread;
pub use single_thread::SingleThreadHelper;
