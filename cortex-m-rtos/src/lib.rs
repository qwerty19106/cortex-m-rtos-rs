#![feature(maybe_uninit_ref)]
#![feature(maybe_uninit_extra)]
#![feature(arbitrary_self_types)]
#![feature(negative_impls)]
#![feature(arbitrary_enum_discriminant)]
#![feature(const_fn)]
// TODO: remove it when const_generics will be complete
#![allow(incomplete_features)]
#![feature(const_generics)]
#![feature(const_panic)]
#![feature(const_in_array_repeat_expressions)]
#![feature(cell_update)]
#![feature(exclusive_range_pattern)]
#![feature(optin_builtin_traits)]
#![feature(naked_functions)]
#![feature(asm)]
#![feature(extern_types)]
#![feature(const_precise_live_drops)]
#![feature(const_fn_fn_ptr_basics)]
#![feature(unsafe_block_in_unsafe_fn)]
#![deny(unsafe_op_in_unsafe_fn)]
#![no_std]

pub mod common;
pub mod os;
pub mod primitive;
