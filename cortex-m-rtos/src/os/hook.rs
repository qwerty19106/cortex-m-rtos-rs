use core::cell::Cell;

use crate::os::token::*;

// TODO: rewrite it with Variadic Generics!
// Now it is blocked on https://github.com/rust-lang/rfcs/issues/376,
// https://github.com/rust-lang/rfcs/pull/2775

macro_rules! define_hook {
    (
        $name: ident; $( $arg: ident: $t:tt ),+
    ) => {
        /// Singleton for event handler
        ///
        /// No handler set by default. Handler can be set at start kernel only.
        /// Handler can be called at any moment (even before kernel starting).
        ///
        /// # Safety
        /// It is thread-safe by design.
        ///
        /// # Runtime overhead
        /// The `call` method takes 3 tick even without handler.
        /// It must be used only for tests and debugging.
        ///
        /// # Memory layout
        /// Size of it equals 4 bytes for supported targets.
        #[repr(transparent)]
        pub struct $name<$( $t: ?Sized ),+>(Cell<Option<fn($( &$t ),+)>>);

        // SAFETY: it is safe by design.
        unsafe impl<$( $t: ?Sized ),+> Sync for $name<$( $t ),+> {}

        impl<$( $t: ?Sized ),+> $name<$( $t ),+> {
            /// Construct it with no handler
            pub const fn new() -> Self {
                Self(Cell::new(None))
            }
            /// Set event handler during kernel starting
            pub fn set(&'static self, func: fn($( &$t ),+), _token: KernelStartingToken) {
                self.0.set(Some(func));
                // SAFETY: synchronization is not required because it will be performed at kernel started.
            }
            /// Call event handler if it is set
            #[inline]
            pub fn call(&'static self, $( $arg: &$t ),+) {
                let inner = self.0.get();
                match inner {
                    None => {}
                    Some(func) => func($( $arg ),+),
                }
            }
        }
    }
}

define_hook!(Hook1;
    arg1: T1);

define_hook!(Hook2;
    arg1: T1, arg2: T2);

define_hook!(Hook3;
    arg1: T1, arg2: T2, arg3: T3);

define_hook!(Hook4;
    arg1: T1, arg2: T2, arg3: T3, arg4: T4);

define_hook!(Hook5;
    arg1: T1, arg2: T2, arg3: T3, arg4: T4, arg5: T5);

define_hook!(Hook6;
    arg1: T1, arg2: T2, arg3: T3, arg4: T4, arg5: T5, arg6: T6);
