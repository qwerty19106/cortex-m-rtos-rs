use core::{
    mem::{size_of, MaybeUninit},
    ptr::{null, NonNull},
};

#[cfg(feature = "stack-check")]
use core::sync::atomic::{compiler_fence, Ordering};

#[cfg(feature = "unprivileged-thread-mode")]
use cortex_m::register::control;

use unsafe_unwrap::UnsafeUnwrap;

use crate::{
    common::*,
    os::{core_cm::*, token::*},
};

#[cfg(feature = "stack-check")]
use crate::os::{svcall::*, KERNEL};

pub const THREAD_STACK_MIN_SIZE: usize = 64;

/// Memory for thread stack with size `SIZE_BY_U32` bytes
///
/// Note! Size should be aligned on 8 bytes. Thus `SIZE_BY_U32` % 2 == 0!
///
/// Note! Feature `"stack-check"` decrease actual avalible stack of thread on 4 bytes.
#[repr(C, align(8))]
struct ThreadStack<const SIZE_BY_U32: usize>([u32; SIZE_BY_U32]);

impl<const SIZE_BY_U32: usize> ThreadStack<SIZE_BY_U32> {
    #[track_caller]
    const fn new() -> Self {
        if (SIZE_BY_U32 % 2) != 0 {
            panic!("Thread stack size should be aligned on 8 bytes");
        }

        if SIZE_BY_U32 * 4 < THREAD_STACK_MIN_SIZE {
            panic!("Thread stack size should be more THREAD_STACK_MIN_SIZE bytes");
        }

        Self([0; SIZE_BY_U32])
    }
}

/// Thread function
// TODO: add T: Send.
// Now rustc not support it in type aliases!
pub type ThreadFunc<T> = fn(arg: T, token: ThreadToken) -> !;

/// System Thread
///
/// NOTE! We not make `Thread` as const generics to one can send `Thread` with any stack size into `Kernel`.
/// Wherein `SIZE` used on thread initialization only and not inflience to performance.
///
/// # Memory layout
/// Field **sp** placed first and struct is **#[repr(C)]**.
/// Thus pointer of struct equals to pointer of **sp**.
#[derive(Debug)]
#[repr(C)]
pub struct Thread {
    /// Current stack pointer
    pub(crate) sp: *const u32,
    /// Stack memory
    pub(crate) stack: ArrayBox<u32>,
    /// Thread Priority
    pub(crate) priority: Priority,

    next_thread: Option<NonNull<Self>>,
}

impl NextField for Thread {
    #[inline]
    fn next(&mut self) -> &mut Option<NonNull<Self>> {
        &mut self.next_thread
    }
}

/// Stack Magic Word (Stack Base)
#[cfg(feature = "stack-check")]
const STACK_MAGIC_WORD: u32 = 0xE25A2EA5;

/// Stack Fill Pattern
#[cfg(feature = "stack-watermark")]
const STACK_FILL_PATTERN: u32 = 0xCCCCCCCC;

impl Thread {
    fn new(stack: ArrayBox<u32>, priority: Priority) -> Self {
        Self {
            sp: null(),
            stack,
            priority,
            next_thread: None,
        }
    }

    #[inline]
    pub fn get_sp(&self) -> *const u32 {
        self.sp
    }

    extern "C" fn worker<T: Send>(
        arg_ptr: *mut Option<T>,
        func_ptr: *mut Option<ThreadFunc<T>>,
    ) -> ! {
        // Take argument
        let mut arg_own = unsafe { Own::new_unchecked(arg_ptr) };
        let arg = arg_own.take();
        let arg = unsafe { arg.unsafe_unwrap() };

        // Get function
        let func_own = unsafe { Own::new_unchecked(func_ptr) };
        let func = unsafe { func_own.unsafe_unwrap() };

        #[cfg(feature = "unprivileged-thread-mode")]
        {
            // Set Unprivileged Thread mode
            let mut reg = control::read();
            if reg.npriv() != control::Npriv::Unprivileged {
                reg.set_npriv(control::Npriv::Unprivileged);
                unsafe {
                    control::write(reg);
                }
            }
        }

        // Call thread function
        let token = ThreadToken::new();
        func(arg, token)
    }

    fn init_stack<T: Send>(
        self: &mut Own<Self>,
        arg: Own<Option<T>>,
        func: Own<Option<ThreadFunc<T>>>,
    ) {
        let stack_size_by_u32 = self.stack.size();

        // Initialize stack watermark
        #[cfg(feature = "stack-watermark")]
        {
            for v in self.stack.iter_mut() {
                *v = STACK_FILL_PATTERN;
            }
        }

        // Set current stack pointer to end of stack, and give 64 bytes offset for registers.
        self.sp = &self.stack[stack_size_by_u32 - 16];

        // Initialize stack registers (R4..R11, R0..R3, R12, LR, PC, xPSR)
        // It will be loaded on first switch context to this thread.
        let registers = &mut self.stack[(stack_size_by_u32 - 16)..];

        for i in 0..7 {
            registers[i] = 0; // R4..R11
        }

        // Thread function arguments
        registers[8] = arg.as_ptr() as u32; // R0
        registers[9] = func.as_ptr() as u32; // R1
        registers[10] = 0; // R2
        registers[11] = 0; // R3

        registers[12] = 0; // R12
        registers[13] = 0; //(uint32_t)osThreadExit;   // LR
        registers[14] = Thread::worker::<T> as u32; // PC
        registers[15] = XPSR_INIT_VAL; // xPSR

        // Initialize stack check
        #[cfg(feature = "stack-check")]
        {
            // Prevent reordering by compiler!
            compiler_fence(Ordering::SeqCst);

            // SAFETY: if stack_size_by_u32 == THREAD_STACK_MIN_SIZE, then R4 register will be rewritten.
            // But it should not affect on starting thread because R4 not used by Thread::worker function.
            self.stack[0] = STACK_MAGIC_WORD;
        }
    }

    /// Check stack of Thread.
    #[cfg(feature = "stack-check")]
    pub(crate) fn ring0_stack_check(&self, _token: Ring0Token) {
        // SAFETY: Prevent bounds check! 0 is true index for self.stack.
        let stack_sheck = unsafe { self.stack.get_unchecked(0) };

        // TODO: сейчас self.stack игнорирует поле stack_check
        // Используем адресную арифметику чтобы к нему обратиться.
        // Удалить когда избавимся от этого поля
        let stack_sheck = unsafe { &*(stack_sheck as *const u32).offset(-1) };

        if *stack_sheck != STACK_MAGIC_WORD {
            panic!("Stack overflow");
        }
    }

    #[cfg(feature = "stack-check")]
    extern "C" fn svc_stack_check() {
        let token = SVCallToken::new();
        let current = KERNEL.svc_current_thread_ref(token);
        current.ring0_stack_check(token.into());
    }

    // TODO: rename
    #[cfg(feature = "stack-check")]
    pub(crate) extern "C" fn svc_pendsv_stack_check(current: &Thread) {
        let token = SVCallToken::new();
        current.ring0_stack_check(token.into());
    }

    /// Check stack of current running Thread.
    #[cfg(feature = "stack-check")]
    #[inline]
    pub fn stack_check(token: ThreadToken) {
        svc0n(Self::svc_stack_check, token);
    }

    /*#[allow(dead_code)]
    pub(crate) fn svc_get_priority(self: &Own<Self>) -> Priority {
        self.priority
    }

    #[allow(dead_code)]
    pub(crate) fn svc_set_priority(self: &mut Own<Thread>, priority: Priority) {
        //if self.state == ThreadState::Terminated
        if self.priority != priority {
            self.priority = priority;
            //osRtxThreadListSort(thread);
            //osRtxThreadDispatch(NULL);
        }
    }*/
}

pub struct ThreadMemory<T: Send, const SIZE_BY_U32: usize> {
    stack: ThreadStack<SIZE_BY_U32>,
    thread: MaybeUninit<Thread>,
    arg: Option<T>,
    func: Option<ThreadFunc<T>>,
}

impl<T: Send, const SIZE_BY_U32: usize> ThreadMemory<T, SIZE_BY_U32> {
    pub const fn new() -> Self {
        Self {
            stack: ThreadStack::new(),
            thread: MaybeUninit::uninit(),
            arg: None,
            func: None,
        }
    }

    pub fn to_thread(
        mut self: Own<Self>,
        // Initial thread priority
        priority: Priority,
        // Thread function
        func: ThreadFunc<T>,
        // Argument
        arg: T,
    ) -> Own<Thread> {
        // Store argument
        // SAFETY: transfer self.arg ownership to Own<Option<T>> ownership
        let mut arg_own = unsafe { Own::new_unchecked(&mut self.arg) };
        *arg_own = Some(arg);

        // Store function
        let mut func_own = unsafe { Own::new_unchecked(&mut self.func) };
        *func_own = Some(func);

        // Create thread
        // SAFETY: transfer self.stack ownership to Own<ThreadStack> ownership
        let stack_own = unsafe { Own::new_unchecked(&mut self.stack.0) };
        let stack = ArrayBox::new(stack_own);

        let thread = Thread::new(stack, priority);
        let thread_ref = self.thread.write(thread);

        // SAFETY: transfer self.thread ownership to Own<Thread> ownership
        let mut thread = unsafe { Own::new_unchecked(thread_ref) };

        // Init stack reference
        // TODO: переместить в new
        thread.init_stack(arg_own, func_own);

        thread
    }
}

// Compile-time tests
mod tests {
    use core::mem::align_of;

    use super::*;

    const _: () = assert!(align_of::<ThreadStack<16>>() == 8);
    const _: () = assert!(size_of::<ThreadStack<18>>() == 72);
}
