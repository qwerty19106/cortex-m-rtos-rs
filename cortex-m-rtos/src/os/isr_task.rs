use core::{fmt, mem::size_of};

use crate::os::token::*;
/// Task for ISRQueue
///
/// ISRTask is used to request some code in PendSV mode from ISR mode. Thus this code can call Kernel methods and
/// synchronization is not required.
///
/// # Implementation details
/// It take reference to static synchronization primitive (T) and reference to its pendsv_process method
/// (`extern "C" fn(&'static T)`).
///
/// ISRTask erase types and store references as `*const c_void`. The `process` method call `pendsv_process` over
/// asm code.
///
/// # How to create synchronization primitive?
/// Synchronization primitive must be static variable. Thus it must implement interior mutability through
/// [`UnsafeCell`](https://doc.rust-lang.org/core/cell/struct.UnsafeCell.html)
/// or [atomic](https://doc.rust-lang.org/core/sync/atomic/index.html) types.
///
/// Add to your synchronization primitive method:
/// ```
/// extern "C" fn pendsv_process(&'static self) {
///     let token = unsafe { PendSVToken::new_unchecked() };
///     ...
/// }
/// ```
///
/// Push task to ISRQueue:
/// ```
/// isr_queue.push(ISRTask::new(self, Self::pendsv_process), token)
/// ```
///
/// See [`crate::primitive::isr_to_thread::sa_semaphore`] source code as example.
// TODO: use GAT
// Blocked on https://github.com/rust-lang/rust/issues/44265
#[derive(Clone, Copy)]
pub struct ISRTask {
    object: &'static ISRTaskObject,
    pendsv_process: extern "C" fn(&'static ISRTaskObject),
}

// TODO: replace to c_void when c_void will be implemented as Extern Type
extern "C" {
    type ISRTaskObject;
}

impl ISRTask {
    #[inline]
    pub fn new<T>(object: &'static T, pendsv_process: extern "C" fn(&'static T)) -> Self {
        // Size of ISRTaskObject reference equals size_of::<usize>(). See Extern Types RFC for details
        // https://github.com/rust-lang/rfcs/blob/master/text/1861-extern-types.md

        // TODO: check size at compile-time
        // Blocked on error: use of generic parameter from outer function
        /*const _: () = assert!(
            size_of::<&'static T>() == 4
        );*/

        let object = unsafe { &*(object as *const T as *const _) };

        // TODO: check size at compile-time
        // Blocked on error: use of generic parameter from outer function
        /*const _: () = assert!(
            size_of::<extern "C" fn(&'static T)>() == 4
        );*/

        const _: () = assert!(size_of::<extern "C" fn(&'static ISRTaskObject)>() == 4);

        // SAFETY:
        // Sizes is equals (size_of::<usize>)
        // ABI is the same (extern "C" fn(thin pointer))
        let pendsv_process = unsafe {
            core::mem::transmute::<extern "C" fn(&'static T), extern "C" fn(&'static ISRTaskObject)>(
                pendsv_process,
            )
        };

        // SAFETY: erase type T
        Self {
            object,
            pendsv_process,
        }
    }

    #[inline]
    pub fn run(self, _token: PendSVToken) {
        let pendsv_process = self.pendsv_process;
        pendsv_process(self.object)
    }
}

impl fmt::Debug for ISRTask {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let object_ptr = self.object as *const _ as usize;
        let pendsv_process = self.pendsv_process as usize;

        f.debug_struct("ISRTask")
            .field("object", &object_ptr)
            .field("pendsv_process", &pendsv_process)
            .finish()
    }
}

impl PartialEq for ISRTask {
    fn eq(&self, other: &Self) -> bool {
        let object1_ptr = self.object as *const _ as usize;
        let object2_ptr = self.object as *const _ as usize;

        (object1_ptr == object2_ptr) && (self.pendsv_process == other.pendsv_process)
    }
}

// Compile-time tests
mod tests {
    use core::mem::size_of;

    use super::*;
    const _: () = assert!(size_of::<ISRTask>() == 8);
    const _: () = assert!(size_of::<Option<ISRTask>>() == 8);
}
