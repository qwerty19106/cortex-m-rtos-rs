use core::marker::PhantomData;

use bare_metal::Nr;
use cortex_m::register::control;
pub use cortex_m::interrupt::CriticalSection;

use crate::os::core_cm::ipsr;

/// Token for `main` function and it subroutine
/// 
/// It uses for start kernel only.
/// 
/// # Memory layout
/// `MainToken` is [ZST](https://doc.rust-lang.org/nomicon/exotic-sizes.html#zero-sized-types-zsts) type.
///
/// # Gaurantee
/// The `MainToken` ensures that `main` function is in progress now.
/// See [entry](https://docs.rs/cortex-m-rt/0.6.12/cortex_m_rt/attr.entry.html) for details.
#[derive(Copy, Clone)]
pub struct MainToken(PhantomData<u8>);

impl !Send for MainToken {}
impl !Sync for MainToken {}

impl MainToken {
    pub fn new() -> Self {
        if control::read().spsel() != control::Spsel::Msp {
            panic!("MainToken can be used only from 'main' function!");
        }

        Self(PhantomData)
    }
}

/// Token for thread context
/// 
/// # Memory layout
/// `ThreadToken` is [ZST](https://doc.rust-lang.org/nomicon/exotic-sizes.html#zero-sized-types-zsts) type.
///
/// # Gaurantee
/// The `ThreadToken` ensures that kernel is in thread mode now.
/// See [interrupt](crate::os::Thread) for details.
#[derive(Copy, Clone)]
pub struct ThreadToken(PhantomData<u8>);

impl !Send for ThreadToken {}
impl !Sync for ThreadToken {}

impl ThreadToken {
    #[inline]
    pub fn new() -> Self {
        if control::read().spsel() != control::Spsel::Psp {
            panic!("ThreadToken can be used only from thread context!");
        }

        Self(PhantomData)
    }
}

/// Token for KernelSingleton::start function
/// 
/// # Memory layout
/// `KernelStartingToken` is [ZST](https://doc.rust-lang.org/nomicon/exotic-sizes.html#zero-sized-types-zsts) type.
///
/// # Gaurantee
/// The `KernelStartingToken` ensures that kernel is initialized but not started yet.
/// See [`KernelSingleton::start`](crate::os::KernelSingleton::start) for details.
#[derive(Copy, Clone)]
pub struct KernelStartingToken(PhantomData<u8>);

impl !Send for KernelStartingToken {}
impl !Sync for KernelStartingToken {}

impl KernelStartingToken {
    pub(crate) fn new() -> Self {
        Self(PhantomData)
    }

    pub unsafe fn new_unchecked() -> Self {
        Self(PhantomData)
    }
}

/// Token for SVCall exception handler
/// 
/// # Memory layout
/// `SVCallToken` is [ZST](https://doc.rust-lang.org/nomicon/exotic-sizes.html#zero-sized-types-zsts) type.
///
/// # Gaurantee
/// The `SVCallToken` ensures that kernel is in `SVCall` exception handler.
/// See [exception](https://docs.rs/cortex-m-rt/0.6.12/cortex_m_rt/attr.exception.html) attribute for details.
#[derive(Copy, Clone)]
pub struct SVCallToken(PhantomData<u8>);

impl !Send for SVCallToken {}
impl !Sync for SVCallToken {}

impl SVCallToken {
    pub(crate) fn new() -> Self {
        Self(PhantomData)
    }

    pub unsafe fn new_unchecked() -> Self {
        Self(PhantomData)
    }
}

/// Token for PendSV exception handler
/// 
/// # Memory layout
/// `PendSVToken` is [ZST](https://doc.rust-lang.org/nomicon/exotic-sizes.html#zero-sized-types-zsts) type.
///
/// # Gaurantee
/// The `PendSVToken` ensures that kernel is in `PendSV` exception handler.
/// See [exception](https://docs.rs/cortex-m-rt/0.6.12/cortex_m_rt/attr.exception.html) attribute for details.
#[derive(Copy, Clone)]
pub struct PendSVToken(PhantomData<u8>);

impl !Send for PendSVToken {}
impl !Sync for PendSVToken {}

impl PendSVToken {
    pub unsafe fn new_unchecked() -> Self {
        Self(PhantomData)
    }
}

#[derive(Copy, Clone)]
pub struct Ring0Token(PhantomData<u8>);

impl !Send for Ring0Token {}
impl !Sync for Ring0Token {}

impl Ring0Token {
    pub(crate) fn new() -> Self {
        Self(PhantomData)
    }

    pub unsafe fn new_unchecked() -> Self {
        Self(PhantomData)
    }
}

/// Token with number for device-specific interrupt handler (ISR)
///
/// The `IPSR` register gives one of 16..255 numbers for ISR. `ISRToken` contains one of 0..239 numbers (`IPSR` - 16)
/// according to [`bare_metal::Nr`] trait.
///
/// # Memory layout
/// Size of `ISRTokenNumber` equals 1 bytes.
///
/// # Gaurantee
/// The `ISRToken` ensures that ISR handler is in progress now.
/// See [interrupt](https://docs.rs/cortex-m-rt/0.6.12/cortex_m_rt/attr.interrupt.html) attribute for details.
///
/// Also it ensures that current ISR number equal `token.irqn()` value.
#[derive(Copy, Clone, PartialEq, Eq)]
pub struct ISRTokenNumber(u8);

impl !Send for ISRTokenNumber {}
impl !Sync for ISRTokenNumber {}

/// Zero-overhead token for device-specific interrupt handler (ISR)
///
/// # Memory layout
/// `ISRToken` is [ZST](https://doc.rust-lang.org/nomicon/exotic-sizes.html#zero-sized-types-zsts) type.
///
/// # Gaurantee
/// The `ISRToken` ensures that kernel is in device-specific interrupt handler.
/// See [interrupt](https://docs.rs/cortex-m-rt/0.6.12/cortex_m_rt/attr.interrupt.html) attribute for details.
#[derive(Copy, Clone)]
pub struct ISRToken(PhantomData<u8>);

impl !Send for ISRToken {}
impl !Sync for ISRToken {}

impl ISRTokenNumber {
    /// Unchecked constructor for ISRToken
    ///
    /// # Runtime overhead
    /// The method is zero overhead.
    ///
    /// # Safety
    /// The method must be called only from device-specific
    /// [interrupt](https://docs.rs/cortex-m-rt/0.6.12/cortex_m_rt/attr.interrupt.html) handler.
    /// The `isr.nr()` must be equals current ISR number.
    ///
    /// # Example
    /// ```
    /// use stm32f1xx_hal::stm32::{interrupt, Interrupt}
    ///
    /// #[interrupt]
    /// fn TIM1_UP() {
    ///     let token = unsafe { ISRToken::new_unchecked(Interrupt::TIM1_UP) };
    ///     ...
    /// }
    /// ```
    // TODO: add const
    // Blocked on RFC 2632, see
    // https://github.com/rust-lang/rust/issues/67792 and https://github.com/rust-lang/rust/issues/67794 for details
    pub unsafe fn new_unchecked(isr: impl Nr) -> Self {
        Self(isr.nr())
    }

    pub const unsafe fn new_unchecked_const(irqn: u8) -> Self {
        Self(irqn)
    }

    /// Checked constructor for ISRToken
    ///
    /// # Runtime overhead
    /// The method loads `IPSR` register and check its value. It gives overhead of several cycles per `new`call.
    ///
    /// # Panics
    /// Panics if it called not from device-specific [interrupt](https://docs.rs/cortex-m-rt/0.6.12/cortex_m_rt/attr.interrupt.html)
    /// handler.
    ///
    /// # Example
    /// ```
    /// use stm32f1xx_hal::stm32::{interrupt, Interrupt}
    ///
    /// #[interrupt]
    /// fn TIM1_UP() {
    ///     let token = unsafe { ISRToken::new_unchecked(Interrupt::TIM1_UP) };
    ///     ...
    /// }
    /// ```
    pub fn new() -> Self {
        // ISRToken gaurantee that IPSR register takes values 16-255. Thus oveflow does not occur!
        let irqn = ipsr();

        match irqn {
            0 => panic!("Call ISRToken::get() from Thread mode is not allowed"),
            1..15 => panic!("Call ISRToken::get() from System exception handler is not allowed"),
            // SAFETY: Oveflow on substraction does not occur, see above!
            irqn => Self(irqn - 16),
        }
    }

    /// ISR number
    ///
    /// Return ISR number according to [`bare_metal::Nr`] trait.
    pub const fn irqn(&self) -> u8 {
        self.0
    }
}

impl From<KernelStartingToken> for Ring0Token {
    fn from(_item: KernelStartingToken) -> Self {
        Ring0Token::new()
    }
}

impl From<SVCallToken> for Ring0Token {
    fn from(_item: SVCallToken) -> Self {
        Ring0Token::new()
    }
}

impl From<PendSVToken> for Ring0Token {
    fn from(_item: PendSVToken) -> Self {
        Ring0Token::new()
    }
}

impl From<ISRTokenNumber> for ISRToken {
    fn from(_item: ISRTokenNumber) -> Self {
        ISRToken(PhantomData)
    }
}
