/// xPSR_Initialization Value
pub(crate) const XPSR_INIT_VAL: u32 = 0x01000000;

// Stack Frame:
//  - Extended: S16-S31, R4-R11, R0-R3, R12, LR, PC, xPSR, S0-S15, FPSCR
//  - Basic:             R4-R11, R0-R3, R12, LR, PC, xPSR

/// Stack Frame Initialization Value (EXC_RETURN[7..0])
//#if (DOMAIN_NS == 1)
//#define STACK_FRAME_INIT_VAL    0xBCU
//#else
//pub const STACK_FRAME_INIT_VAL: u8 = 0xFD;
//#endif

#[inline]
pub(crate) fn ipsr() -> u8 {
    let ipsr: u32;
    unsafe {
        asm!(
            "mrs {}, IPSR",
            lateout(reg_thumb) ipsr,
            options(pure, nomem, nostack)
        );
    }
    ipsr as u8
}
