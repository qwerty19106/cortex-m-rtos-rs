use core::cell::UnsafeCell;
use core::sync::atomic::{compiler_fence, AtomicUsize, Ordering};

use unsafe_unwrap::UnsafeUnwrap;

use crate::{
    common::*,
    os::{token::*, *},
};


// Compile-time check of isr-queue-size-xxx features
const _: () = {
    let count = cfg!(feature = "isr-queue-size-3") as u8
        + cfg!(feature = "isr-queue-size-7") as u8
        + cfg!(feature = "isr-queue-size-15") as u8
        + cfg!(feature = "isr-queue-size-31") as u8
        + cfg!(feature = "isr-queue-size-63") as u8
        + cfg!(feature = "isr-queue-size-127") as u8
        + cfg!(feature = "isr-queue-size-255") as u8;

    if count == 0 {
        panic!("One of features should be enabled: isr-queue-size-xxx, where xxx is 3,7,15,...255")
    }

    if count > 1 {
        panic!("Only one of features should be enabled: isr-queue-size-xxx, where xxx is 3,7,15,...255")
    }
};

/// ISR Post Processing Queue (FIFO).
///
/// It is intrusive bounded MPSC queue.
/// Intrusive means that dynamic allocation is not required.
/// Bounded means that container can takes bounded number of tasks (see ISRQueueMemory definition).
///
/// # Working principle
/// Interrupt processing is deferred and is occured by the following scheme:
/// - interrupts call `ISRQueue::push` which add task into `ISRQueue`, and then set `PendSV` flag.
/// - `PendSV` exception have least priority.
/// - When all interrupts are processed, `PendSV` handler will be called.
/// - `PendSV` handler extracts and processes all tasks from the `ISRQueue`, and finally performs context switching, if necessary.
///
/// Thus, the `push` and` pop` methods should have the following requirements:
/// - `push` can be called only from interrupts.
/// - `push` can be preempted by other `push`.
/// - `pop` can be called only from `PendSV` exception. This only one `pop` called simultaneously by design.
/// - `push` can preempt `pop`.
/// - `pop` can not preempt `push`.
///
/// The some important conclusion:
/// - `push` or `pop` is executed: `head` can not be changed by any exception or interrupt!
/// - `push` or `pop` is executed: `tail` can be increased (but not decrease!) by other interrupt.
///
/// # Implementation details:
/// - SIZE (`self.ring_buffer.size()`) is 2^n.
/// - there is dummy task, which is unused. Thus real size equals SIZE - 1.
/// - `self.head & (SIZE - 1)` - index of first pushed task.
/// - `self.tail & (SIZE - 1)` - index of first free task (after last pushed task).
/// - queue is empty when `self.tail == self.head`.
/// - queue is full when `self.tail + 1 == self.head + SIZE`.
/// - `self.tail` and `self.head` can be overflowed.
pub struct ISRQueue {
    /// Head index
    head: AtomicUsize,
    /// Tail index
    tail: AtomicUsize,
    /// Ring buffer for queue data
    ring_buffer: UnsafeCell<[Option<ISRTask>; Self::SIZE]>,

    #[cfg(feature = "isr-queue-hooks")]
    pub hooks: ISRQueueHooks,
}

unsafe impl Sync for ISRQueue {}

impl ISRQueue {
    /// Size of inner buffer
    pub const SIZE: usize = {
        let mut size = 0;

        if cfg!(feature = "isr-queue-size-3") {
            size = 4;
        }

        if cfg!(feature = "isr-queue-size-7") {
            size = 8;
        }

        if cfg!(feature = "isr-queue-size-15") {
            size = 16;
        }

        if cfg!(feature = "isr-queue-size-31") {
            size = 32;
        }

        if cfg!(feature = "isr-queue-size-63") {
            size = 64;
        }

        if cfg!(feature = "isr-queue-size-127") {
            size = 128;
        }

        if cfg!(feature = "isr-queue-size-255") {
            size = 256;
        }

        size
    };

    const fn new() -> Self {
        Self {
            head: AtomicUsize::new(0),
            tail: AtomicUsize::new(0),
            ring_buffer: UnsafeCell::new([None; Self::SIZE]),

            #[cfg(feature = "isr-queue-hooks")]
            hooks: ISRQueueHooks::new(),
        }
    }

    #[cfg(feature = "isr-queue-hooks")]
    fn get_buffer(&'static self, _token: &CriticalSection) -> &[Option<ISRTask>] {
        // SAFETY: interior mutability is allowed in interrupt-free critical section
        unsafe { &*self.ring_buffer.get() }
    }

    /// Unsafe mutable access to task by index
    ///
    /// #Safety
    /// Index should be valid.
    /// You should have exclusive access to task.
    unsafe fn task_mut(&'static self, index: usize) -> &mut Option<ISRTask> {
        // SAFETY: Interior mutability allowed by method invariants.
        let ring_buffer = unsafe { &mut *self.ring_buffer.get() };

        // SAFETY: Prevent bounds check! The index is valid (see push and run_all methods).
        unsafe { ring_buffer.get_unchecked_mut(index) }
    }

    /// Try push task into queue
    ///
    /// Return false if queue is full.
    ///
    /// # Safety
    /// This method can be called only from interrupt handler.
    /// It is derived by ISRToken.
    pub fn push(&'static self, task: ISRTask, _token: ISRToken) -> bool {
        #[cfg(feature = "isr-queue-hooks")]
        cortex_m::interrupt::free(|token| {
            self.hooks
                .before_push
                .call(&self.head, &self.tail, self.get_buffer(token), &task);
        });

        // self.head is fixed here, see ISRQueue description for details.
        let head = self.head.load(Ordering::Relaxed);

        // SAFETY: overflow is allowed, see ISRQueue description for details.
        let limit = head + Self::SIZE - 1;

        let (res, old_tail) = atomic_usize_inc_limit_extended(&self.tail, limit);
        if !res {
            // Queue is full! Do nothing
        } else {
            // Success! We have exclusive access to self.data[old_tail]!
            // Store task and return.

            // SAFETY: optimization. The SIZE equals 2^n, thus a % b == a & b.
            let valid_tail = old_tail & (Self::SIZE - 1);

            // SAFETY: index is valid.
            // SAFETY: CAS above derive exclusive access to this item of buffer. See ISRQueue description for details.
            let task_ref = unsafe { self.task_mut(valid_tail) };
            *task_ref = Some(task);
        }

        #[cfg(feature = "isr-queue-hooks")]
        cortex_m::interrupt::free(|token| {
            self.hooks
                .after_push
                .call(&self.head, &self.tail, self.get_buffer(token), &task, &res);
        });

        res
    }

    /// Pop task from queue and run it
    ///
    /// New head value will be stored into self.head and then returned.
    ///
    /// # Safety
    /// Queue should be non-empty (tail != head)
    /// Head should equals to self.head
    #[inline]
    unsafe fn pop_and_run(&'static self, mut head: usize, token: PendSVToken) -> usize {
        // SAFETY: optimization. The SIZE equals 2^n, thus a % b == a & b.
        let valid_head = head & (Self::SIZE - 1);

        // SAFETY: index is valid.
        // SAFETY: self.head is fixed by PendSVToken. See ISRQueue description for details.
        let task_ref = unsafe { self.task_mut(valid_head) };

        // Read task
        let task = task_ref.take();
        let task = unsafe { task.unsafe_unwrap() }; // SAFETY: API ensure that task in not None

        // Increase head
        // SAFETY: overflow is allowed, see ISRQueue description for details.
        head += 1;

        // NOTE! All loads and stores always complete in program order, even if the first is buffered (for Cortex-M3
        // and Cortex-M4). Thus dmb instruction between reading task and writing head is not required.
        // See 3.5 section for details:
        // https://static.docs.arm.com/dai0321/a/DAI0321A_programming_guide_memory_barriers_for_m_profile.pdf?_ga=2.223101657.334723499.1587480748-1742183230.1584629625
        // TODO: check it for other Cortex-M.

        // Prevent reading task and writing self.head reordering by compiler!
        compiler_fence(Ordering::SeqCst);

        // Store head
        // NOTE! It placed into loop to process very many ISR during one PendSV.
        // For example, 3 ISR (add 3 task) -> PendSV processed 1 task -> preempted by ISR (add 1 task) -> ...
        self.head.store(head, Ordering::Relaxed);

        // Prevent compiler reordering as call hook should be called after self.head store
        #[cfg(feature = "isr-queue-hooks")]
        compiler_fence(Ordering::SeqCst);

        // Process task
        #[cfg(feature = "isr-queue-hooks")]
        cortex_m::interrupt::free(|token| {
            self.hooks
                .before_run_task
                .call(&self.head, &self.tail, self.get_buffer(token), &task);
        });

        task.run(token);

        #[cfg(feature = "isr-queue-hooks")]
        cortex_m::interrupt::free(|token| {
            self.hooks
                .after_run_task
                .call(&self.head, &self.tail, self.get_buffer(token), &task);
        });

        head
    }

    /// Pop all tasks from queue and run it consequentially
    ///
    /// New head value will be returned
    pub fn run_all(&'static self, token: PendSVToken) {
        #[cfg(feature = "isr-queue-hooks")]
        cortex_m::interrupt::free(|token| {
            self.hooks
                .before_run_all
                .call(&self.head, &self.tail, self.get_buffer(token));
        });

        // self.head is fixed here, see ISRQueue description for details.
        let mut head = self.head.load(Ordering::Relaxed);

        loop {
            let tail = self.tail.load(Ordering::Relaxed);
            if tail == head {
                // Queue is empty
                break;

                // Other ISR can preempt this PendSV and call push (increase `self.tail`).
                // Then PendSV will be set and new task will be processed on next PendSV call.
                // Thus we should not atomically check self.tail here.
            }

            // SAFETY: self.head is fixed here
            // SAFETY: queue is not empty
            head = unsafe { self.pop_and_run(head, token) };
        }

        #[cfg(feature = "isr-queue-hooks")]
        cortex_m::interrupt::free(|token| {
            self.hooks
                .after_run_all
                .call(&self.head, &self.tail, self.get_buffer(token));
        });
    }
}

#[no_mangle]
pub static ISR_QUEUE: ISRQueue = ISRQueue::new();

pub(crate) extern "C" fn os_pendsv_handler() {
    let token = unsafe { PendSVToken::new_unchecked() };
    ISR_QUEUE.run_all(token);
}

#[cfg(feature = "isr-queue-hooks")]
pub struct ISRQueueHooks {
    /// The push hook (before)
    ///
    /// Arguments:
    /// - head
    /// - tail
    /// - ring_buffer
    /// - task
    pub before_push: Hook4<AtomicUsize, AtomicUsize, [Option<ISRTask>], ISRTask>,

    /// The push hook (after)
    ///
    /// Arguments:
    /// - head
    /// - tail
    /// - ring_buffer
    /// - task
    /// - result
    pub after_push: Hook5<AtomicUsize, AtomicUsize, [Option<ISRTask>], ISRTask, bool>,

    /// The run_all hook (before)
    ///
    /// Arguments:
    /// - head
    /// - tail
    /// - ring_buffer
    pub before_run_all: Hook3<AtomicUsize, AtomicUsize, [Option<ISRTask>]>,

    /// The run_all hook (after)
    ///
    /// Arguments:
    /// - head
    /// - tail
    /// - ring_buffer
    pub after_run_all: Hook3<AtomicUsize, AtomicUsize, [Option<ISRTask>]>,

    /// The run task hook (before)
    ///
    /// Arguments:
    /// - head
    /// - tail
    /// - ring_buffer
    /// - task
    pub before_run_task: Hook4<AtomicUsize, AtomicUsize, [Option<ISRTask>], ISRTask>,

    /// The run task hook (after)
    ///
    /// Arguments:
    /// - head
    /// - tail
    /// - ring_buffer
    /// - task
    pub after_run_task: Hook4<AtomicUsize, AtomicUsize, [Option<ISRTask>], ISRTask>,
}

#[cfg(feature = "isr-queue-hooks")]
impl ISRQueueHooks {
    const fn new() -> Self {
        Self {
            before_push: Hook4::new(),
            after_push: Hook5::new(),
            before_run_all: Hook3::new(),
            after_run_all: Hook3::new(),
            before_run_task: Hook4::new(),
            after_run_task: Hook4::new(),
        }
    }
}
