use crate::os::{os_pendsv_handler, KERNEL};

#[cfg(feature = "stack-check")]
use crate::os::Thread;

#[naked]
#[no_mangle]
pub unsafe extern "C" fn SVCall() {
    unsafe {
        asm!("
            MRS     R12,PSP                 // Get PSP

            PUSH    {{R12,LR}}              // Save SP and EXC_RETURN
            LDM     R12,{{R0-R3,R12}}       // Load function parameters and address from stack
            BLX     R12                     // Call service function
            POP     {{R12,LR}}              // Restore SP and EXC_RETURN
            STM     R12,{{R0}}              // Store function return values

            // Analyze KERNEL.inner
            LDR     R3,={}                  // Load address of KERNEL
            LDM     R3,{{R0,R1}}            // Load KERNEL.inner: actual_thread_ptr (R0), thread(R1)
        
            CMP     R0,R1                   // Check if thread switch is required
            IT      EQ
            BXEQ    LR                      // Exit when threads are the same

            // Save old thread context (or idle loop context)
            // Note! actual_thread_ptr != null for SVCall
            MRS     R12,PSP                 // Get PSP
            STMDB   R12!,{{R4-R11}}         // Save R4..R11 to PSP
            STR     R12,[R0]                // Store SP to (*actual_thread_ptr).sp

            // Switch actual_thread_ptr
            STR     R1,[R3]                 // KERNEL.inner: actual_thread_ptr = thread

            // Restore new thread context (or idle loop context)
            CMP     R1,#0                   
            ITEEE   EQ                      // KERNEL.inner: if thread == null()
            POPEQ   {{R4-R11}}              // Save R4..R11 to MSP
            LDRNE   R12,[R1]                // Load SP from thread.sp
            LDMIANE R12!,{{R4-R11}}         // Restore R4..R11 from SP
            MSRNE   PSP,R12                 // Set PSP

            // Set EXC_RETURN
            ITET    EQ                      // KERNEL.inner: if thread == null()
            MOVEQ   LR,#0xFFFFFFF9          // Set Privileged mode & MSP
            MOVNE   LR,#0xFFFFFFFD          // Set Unprivileged mode & PSP
            BXEQ    LR                      // Exit when no new threads
            ",
            sym KERNEL
        );

        #[cfg(feature = "stack-check")]
        asm!("
            // Note! actual_thread_ptr != null for SVCall
            PUSH    {{LR}}
            bl {}                           // Call stack check
            POP     {{LR}}
            ",
            sym Thread::svc_pendsv_stack_check
        );
    }
}

#[naked]
#[no_mangle]
pub unsafe extern "C" fn PendSV() {
    unsafe {
        asm!("
            PUSH    {{LR}}                  // Save EXC_RETURN
            BL      {}                      // Call os_pendsv_handler
            POP     {{LR}}                  // Restore EXC_RETURN

            // Analyze KERNEL.inner
            LDR     R3,={}                  // Load address of KERNEL
            LDM     R3,{{R0,R1}}            // Load KERNEL.inner: actual_thread_ptr (R0), thread(R1)
        
            CMP     R0,R1                   // Check if thread switch is required
            IT      EQ
            BXEQ    LR                      // Exit when threads are the same

            // Note! For PendSV only two variants are possible:
            // 1) thread -> idle
            // 2) idle -> thread
            // Thus actual_thread_ptr==null => thread!= null and vice versa!
            CMP     R0,#0                   

            // Save old thread context (or idle loop context)
            ITEEE   EQ                      // KERNEL.inner: if actual_thread_ptr == null
            PUSHEQ  {{R4-R11}}              // Save R4..R11 to MSP
            MRSNE   R12,PSP                 // Get PSP
            STMDBNE R12!,{{R4-R11}}         // Save R4..R11 to PSP
            STRNE   R12,[R0]                // Store SP to (*actual_thread_ptr).sp

            // Switch actual_thread_ptr
            STR     R1,[R3]                 // KERNEL.inner: actual_thread_ptr = thread

            // Restore new thread context (or idle loop context)
            ITEEE   NE                      // KERNEL.inner: if actual_thread_ptr != null (thread == null)
            POPNE   {{R4-R11}}              // Save R4..R11 to MSP
            LDREQ   R12,[R1]                // Load SP from thread.sp
            LDMIAEQ R12!,{{R4-R11}}         // Restore R4..R11 from SP
            MSREQ   PSP,R12                 // Set PSP

            // Set EXC_RETURN
            ITET    NE                      // KERNEL.inner: if actual_thread_ptr != null (thread == null)
            MOVNE   LR,#0xFFFFFFF9          // Return to MSP
            MOVEQ   LR,#0xFFFFFFFD          // Return to PSP
            ",
            sym os_pendsv_handler,
            sym KERNEL
        );

        #[cfg(feature = "stack-check")]
        asm!("
            IT      EQ                      // KERNEL.inner: if actual_thread_ptr == null
            BXEQ    LR                      // Exit
            PUSH    {{LR}}
            bl {}                           // Call stack check
            POP     {{LR}}
            ",
            sym Thread::svc_pendsv_stack_check
        );
    }
}
