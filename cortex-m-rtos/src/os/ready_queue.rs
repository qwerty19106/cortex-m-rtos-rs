use core::cell::UnsafeCell;

use crate::{
    common::*,
    os::{token::*, *},
};

/// Priority queue for ready threads
///
/// # Runtime overhead
/// `ReadyQueue` is thin wrapper of PriorityQueue.
/// All its methods is inline and should be optimized to zero-cost.
///
/// # Thread safety
/// `ReadyQueue` is `thread-safe` by design. It derives by `Ring0Token` token.
/// Thus `ReadyQueue` implement `Sync`.
#[repr(transparent)]
pub struct ReadyQueue(UnsafeCell<PriorityQueue<Thread>>);

unsafe impl Sync for ReadyQueue {}

impl ReadyQueue {
    pub const fn new() -> Self {
        Self(UnsafeCell::new(PriorityQueue::new()))
    }

    #[inline]
    fn inner(&self, _token: Ring0Token) -> &mut PriorityQueue<Thread> {
        // SAFETY: interior mutability is allowed because `ReadyQueue` is `thread-safe` by design.
        unsafe { &mut *self.0.get() }
    }

    /// Push thread
    #[inline]
    pub fn push(&self, item: Own<Thread>, token: Ring0Token) {
        let priority = item.priority;
        self.inner(token).push(priority, item);
    }

    /// Pop thread with max priority
    #[inline]
    pub fn pop(&self, token: Ring0Token) -> Option<Own<Thread>> {
        self.inner(token).pop()
    }

    /// Pop thread with some priority
    #[inline]
    pub fn pop_by_priority(&self, priority: Priority, token: Ring0Token) -> Option<Own<Thread>> {
        self.inner(token).pop_by_priority(priority)
    }
}

/// READY_QUEUE - priority queue for ready threads
///
/// See [crate::os::ReadyQueue] description for details.
pub static READY_QUEUE: ReadyQueue = ReadyQueue::new();
