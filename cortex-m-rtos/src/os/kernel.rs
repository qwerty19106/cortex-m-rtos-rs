use core::{
    cell::{Cell, UnsafeCell},
    mem,
    ptr::null_mut,
    sync::atomic::{compiler_fence, AtomicBool, Ordering},
};

use cortex_m::{peripheral::SCB, Peripherals};

#[cfg(feature = "unprivileged-thread-mode")]
use cortex_m::register::control;

use cortex_m_rt::pre_init;

use unsafe_unwrap::UnsafeUnwrap;

use crate::{
    common::*,
    os::{svcall::*, token::*, *},
};

/// Mutable Kernel Inner
///
/// - `actual_thread_ptr` is actual running thread.
/// - `thread` is thread which should run as soon as possible.
///
/// If `actual_thread_ptr` not equals `thread` then context will be switched at the end of `system exteption` handler.
/// For details, see thumbv7m.rs file.
///
/// Note! In thread mode `thread` equals to `actual_thread_ptr` always.
///
/// Note! `#[repr(C)]` because it used from assembler code (for example, see thumbv7m.rs).
/// The `actual_thread_ptr` changed by context switching only.
#[repr(C)]
struct KernelInner {
    /// Actual running thread pointer
    actual_thread_ptr: *mut Thread,
    /// Thread which should run as soon as possible
    thread: Option<Own<Thread>>,
    // TODO: test optimization with thread_priority field
    // Priority of `self.thread`
    //thread_priority: Option<Priority>,
}

/// OS kernel information structure
///
/// Kernel define `Ring0` mode. It include `system exteption` handlers (`SVCall`, `PendSV`) and Kernel starting mode
/// (see [`Kernel::start`] method for details).
///
/// `SVCall` and `PendSV` exceptions have the same priority and could not preempt each other.
/// Interrupts is disabled before Kernel is started.
/// Thus we don't need to change `inner` field atomically in `Ring0` mode.
///
/// Note! Do not use `Kernel` in user code. Use `KERNEL` singleton instead of.
///
/// # Safety
/// `Kernel` is `thread-safe` by design. It derives by tokens (`Ring0Token`, `SVCallToken`, `PendSVToken` or
/// `KernelStartingToken`). Thus `Kernel` implement `Sync`.
///
/// Note! Kernel sets SVCall and PendSV priority as lowest (0xFF).
/// SVCall will be performed first because SVCall number lower than PendSV number.
/// See [Exception](https://docs.rs/cortex-m/0.6.2/cortex_m/peripheral/scb/enum.Exception.html) for details.
#[repr(C)]
pub struct Kernel {
    inner: UnsafeCell<KernelInner>,

    /// Flag that PendSV is masked
    ///
    /// If true then PendSV will not be called by `Self::isr_request_pendsv` function. But PendSV will be called after
    /// discarding this flag.
    /// See `Self::isr_request_pendsv` function for details.
    pub(crate) is_pendsv_masked: Cell<bool>,

    /// Flag that PendSV was requested and will be called in the future.
    /// See `Self::isr_request_pendsv` function for details.
    pub(crate) is_pendsv_requested: AtomicBool,

    #[cfg(feature = "custom-idle-function")]
    /// Custom idle function
    ///
    /// It will be called in idle loop (when no rinning thread)
    idle_func: Cell<Option<fn()>>,
}

// SAFETY: Kernel is thread-safe by design (ensures by tokens)
unsafe impl Sync for Kernel {}

impl Kernel {
    pub(crate) const fn new() -> Self {
        Self {
            inner: UnsafeCell::new(KernelInner {
                actual_thread_ptr: null_mut(),
                thread: None,
            }),
            is_pendsv_masked: Cell::new(true),
            is_pendsv_requested: AtomicBool::new(false),

            #[cfg(feature = "custom-idle-function")]
            idle_func: Cell::new(None),
        }
    }

    #[inline]
    pub(crate) fn isr_request_pendsv(&'static self, _token: ISRToken) {
        if self.is_pendsv_masked.get() {
            // SAFETY: Relaxed because ISR start and end include memory barriers.
            if !self.is_pendsv_requested.load(Ordering::Relaxed) {
                self.is_pendsv_requested.store(true, Ordering::Relaxed);
            }
        } else {
            SCB::set_pendsv();
        }
    }

    /// Get custom idle function
    #[cfg(feature = "custom-idle-function")]
    #[inline]
    pub(crate) fn idle_func(&'static self, _token: Ring0Token) -> Option<fn()> {
        self.idle_func.get()
    }

    /// Set custom idle function at Kernel starting
    #[cfg(feature = "custom-idle-function")]
    #[inline]
    pub fn set_idle_func(&'static self, idle_func: fn(), _token: KernelStartingToken) {
        self.idle_func.set(Some(idle_func));
    }

    /// Get mutable reference to ThreadRunInfo
    #[inline]
    fn inner(&'static self, _token: Ring0Token) -> &mut KernelInner {
        // SAFETY: interior mutability allowed by Ring0Token (see Kernel and KernelInner description).
        unsafe { &mut *self.inner.get() }
    }

    /// Get reference to current thread (SVCall mode)
    #[inline]
    pub fn svc_current_thread_ref(&'static self, token: SVCallToken) -> &Thread {
        let inner = self.inner(token.into());

        // SAFETY: SVCallToken ensure that `thread` field is not None.
        unsafe { inner.thread.as_ref().unsafe_unwrap() }
    }

    /// Get reference to current thread (Ring0 mode)
    #[inline]
    pub fn ring0_current_thread_ref(&'static self, token: Ring0Token) -> Option<&Own<Thread>> {
        let inner = self.inner(token.into());
        inner.thread.as_ref()
    }

    // /// Switch thread from system handler
    // ///
    // /// Note! Kernel will switch context at the end of system handler.
    // #[inline]
    // pub fn ring0_switch(&'static self, thread: Own<Thread>, token: Ring0Token) -> Option<Own<Thread>> {
    //     let inner = self.inner(token);
    //     inner.thread.replace(thread)
    // }

    // /// Switch thread from SVCall
    // ///
    // /// Note! Kernel will switch context at the end of SVCall.
    // #[inline]
    // pub fn svc_switch(&'static self, thread: Own<Thread>, _token: SVCallToken) -> Own<Thread> {
    //     let inner = self.inner(token);
    //     let old = inner.thread.replace(thread);

    //     // SAFETY: SVCallToken ensure that `thread` field is not None.
    //     unsafe { old.unsafe_unwrap() }
    // }

    /// Suspend current thread from SVCall and return it
    ///
    /// Note! Kernel will start idle loop at the end of SVCall.
    #[inline]
    pub fn svc_suspend_current(&'static self, token: SVCallToken) -> Own<Thread> {
        let inner = self.inner(token.into());
        let old = mem::replace(&mut inner.thread, None);

        // SAFETY: SVCallToken ensure that `thread` field is not None.
        unsafe { old.unsafe_unwrap() }
    }

    /// Dispatch thread from SVCall
    ///
    /// If thread priority more than current thread priority (or no current thread):
    /// 1) switch thread
    /// 2) push current thread into ready queue
    ///
    /// Else:
    /// push thread into ready queue
    pub(crate) fn svc_dispatch_thread(&'static self, new: Own<Thread>, token: SVCallToken) {
        let inner = self.inner(token.into());

        // SAFETY: SVCallToken ensure that `thread` field is not None.
        let curr = unsafe { inner.thread.as_mut().unsafe_unwrap() };

        if curr.priority < new.priority {
            let old = mem::replace(curr, new);
            READY_QUEUE.push(old, token.into());
        } else {
            READY_QUEUE.push(new, token.into());
        }
    }

    /// Dispatch thread from system exception
    ///
    /// If thread priority more than current thread priority (or no current thread):
    /// 1) switch thread
    /// 2) push current thread into ready queue
    ///
    /// Else:
    /// push thread into ready queue
    pub(crate) fn ring0_dispatch_thread(&'static self, new: Own<Thread>, token: Ring0Token) {
        let inner = self.inner(token);

        match &mut inner.thread {
            Some(curr) => {
                if curr.priority < new.priority {
                    let old = mem::replace(curr, new);
                    READY_QUEUE.push(old, token);
                } else {
                    READY_QUEUE.push(new, token);
                }
            }
            None => {
                inner.thread = Some(new);
            }
        }
    }

    /// Add thread during kernel starting
    pub fn add_thread(&'static self, thread: Own<Thread>, token: KernelStartingToken) {
        self.ring0_dispatch_thread(thread, token.into());
    }

    extern "C" fn svc_run_thread(thread: Own<Thread>) {
        let token = SVCallToken::new();
        KERNEL.svc_dispatch_thread(thread, token);
    }

    /// Run thread from Thread Mode
    ///
    /// # Important
    /// Current thread can be preempted if it has lower priority.
    pub fn run_thread(thread: Own<Thread>, token: ThreadToken) {
        svc1n_box(Kernel::svc_run_thread, thread, token);
    }

    /// Switch to next ready thread with the same priority.
    #[allow(dead_code)]
    pub(crate) fn svc_yield(&'static self, token: SVCallToken) {
        let inner = self.inner(token.into());

        // SAFETY: SVCallToken ensure that `thread` field is not None.
        let curr = unsafe { inner.thread.as_mut().unsafe_unwrap() };

        match READY_QUEUE.pop_by_priority(curr.priority, token.into()) {
            None => {}
            Some(new) => {
                let old = mem::replace(curr, new);
                READY_QUEUE.push(old, token.into())
            }
        }
    }

    /// Start kernel
    ///
    /// Init and start kernel. The `on_start` closure will be called before start kernel.
    /// Note! Can be called only from main function (derived by MainToken).
    pub fn start<F: FnOnce(Peripherals, KernelStartingToken)>(
        &'static self,
        cp: Peripherals,
        _token: MainToken,
        on_start: F,
    ) -> ! {
        let token = KernelStartingToken::new();

        // Run user function
        on_start(cp, token);

        // Allow PendSV
        self.is_pendsv_masked.set(false);
        compiler_fence(Ordering::SeqCst);

        // Call PendSV to jump into thread (if needs)
        SCB::set_pendsv();
        cortex_m::asm::isb();

        #[cfg(feature = "unprivileged-thread-mode")]
        {
            // Set Unprivileged Thread mode
            let mut reg = control::read();
            if reg.npriv() != control::Npriv::Unprivileged {
                reg.set_npriv(control::Npriv::Unprivileged);
                unsafe {
                    control::write(reg);
                }
            }
        }

        // Idle loop
        loop {
            // To prevent compiler optimization of loop
            compiler_fence(Ordering::SeqCst);

            #[cfg(feature = "custom-idle-function")]
            match KERNEL.idle_func(token.into()) {
                Some(idle_func) => idle_func(),
                None => {}
            }
        }
    }
}

#[no_mangle]
pub static KERNEL: Kernel = Kernel::new();

#[pre_init]
unsafe fn before_main() {
    // Setup SVCall and PendSV priority
    let scb = unsafe { &*cortex_m::peripheral::SCB::ptr() };
    unsafe {
        scb.shpr[7].write(0xFF); // SVCall
        scb.shpr[10].write(0xFF); // PendSV
    }
}
