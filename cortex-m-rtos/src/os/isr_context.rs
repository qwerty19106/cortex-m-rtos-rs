use core::cell::UnsafeCell;

use crate::os::token::*;

/// Context for interrupt handler
///
/// # Thread safety
/// The `set` method can be called on kernel starting only, and the `get` from ISR only. Any interrupts are masked until
/// the kernel is started, thus synchronization between `get` and `set` is not required.
///
/// The `get` method compare ISR number with own number. Thus only one ISR can get `&mut T` and synchronization on `get`
/// is not required.
// TODO: replace to:
/*
pub struct ISRContext<T: Send, const IRQN: u8> {
    data: Cell<Option<T>>,
}

// Using:
use stm32f1xx_hal::stm32::Interrupt;
static ISRContext<u8, Interrupt::USART1::nr()>
*/
// Blocked on RFC 2632, see
// https://github.com/rust-lang/rust/issues/67792 and https://github.com/rust-lang/rust/issues/67794 for details
pub struct ISRContext<T: Send, const IRQN: u8> {
    data: UnsafeCell<Option<T>>,
}

unsafe impl<T: Send, const IRQN: u8> Sync for ISRContext<T, IRQN> {}

impl<T: Send, const IRQN: u8> ISRContext<T, IRQN> {
    pub const fn new() -> Self {
        Self {
            data: UnsafeCell::new(None),
        }
    }

    /// Set context on kernel starting
    ///
    /// # Runtime overhead
    /// The method must be called only one times. Thus overhead is insignificant.
    ///
    /// # Panics
    /// Panics if context is already set.
    pub fn set(&self, value: T, _: KernelStartingToken) {
        // SAFETY: interior mutability is allowed because `ISRContext` is `thread-safe` by design.
        // See struct description for details.
        let data = unsafe { &mut *self.data.get() };

        match data {
            None => *data = Some(value),
            Some(_) => panic!("ISRContext already set (irqn = {})", IRQN),
        }
    }

    /// Get `&mut T` on context inner value from ISR
    ///
    /// # Runtime overhead
    /// ???
    ///
    /// # Panics
    /// Panics if context is not set.
    /// Panics if the current ISR number is not equals to its own number of `ISRContext`.
    pub fn get<const TOKEN: ISRTokenNumber>(&self) -> Option<&'static mut T> {
        assert_eq!(TOKEN.irqn(), IRQN);

        // SAFETY: interior mutability is allowed because `ISRContext` is `thread-safe` by design.
        // See struct description for details.
        let inner_ref = unsafe { &mut *self.data.get() };
        inner_ref.as_mut()
    }

    pub unsafe fn get_unchecked(&self, _token: ISRToken) -> Option<&'static mut T> {
        // SAFETY: interior mutability is allowed because `ISRContext` is `thread-safe` by design.
        // See struct description for details.
        let inner_ref = unsafe { &mut *self.data.get() };
        
        inner_ref.as_mut()
    }
}
