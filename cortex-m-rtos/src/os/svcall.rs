use crate::{common::Own, os::token::*};

#[cfg(feature = "stack-check")]
#[inline(always)]
pub(crate) fn svc0n(func: extern "C" fn(), _token: ThreadToken) {
    unsafe {
        asm!(
            "svc 0",
            in("r12") func
        );
    }
}

/*#[inline]
pub(crate) fn svc1n<T>(func: extern "C" fn(&T), arg: &T, _token: ThreadToken) {
    unsafe {
        llvm_asm!("
            mov r12, $0
            mov r0, $1
            svc 0
            " :: "r"(func), "r"(arg) : "r0", "r12" : "volatile");
    }
}*/

#[inline(always)]
pub(crate) fn svc1n_static<T>(func: extern "C" fn(&'static T), arg: &'static T, _token: ThreadToken) {
    unsafe {
        asm!(
            "svc 0",
            in("r12") func,
            in("r0") arg,
        );
    }
}

#[inline(always)]
pub(crate) fn svc1n_box<T>(func: extern "C" fn(Own<T>), arg: Own<T>, _token: ThreadToken) {
    let arg = arg.as_ptr() as usize;
    unsafe {
        asm!(
            "svc 0",
            in("r12") func,
            in("r0") arg,
        );
    }
}

/*#[inline]
pub(crate) fn svc1n_mut<T>(func: extern "C" fn(&mut T), arg: &mut T, _token: ThreadToken) {
    let arg = arg as *mut T as usize;
    unsafe {
        llvm_asm!("
            mov r12, $0
            mov r0, $1
            svc 0
            " :: "r"(func), "r"(arg) : "r0", "r12" : "volatile");
        //__svc1(arg as *mut T as usize, func as usize);
    }
}*/

/*
#[inline]
pub(crate) fn svc1u<T>(func: extern "C" fn(&T) -> usize, arg: &T, _token: &ThreadToken) -> usize {
    unsafe { __svc1(arg as *const T as usize, func as usize) }
}

#[inline]
pub(crate) fn svc1b<T>(func: extern "C" fn(&T) -> bool, arg: &T, _token: &ThreadToken) -> bool {
    unsafe { __svc1(arg as *const T as usize, func as usize) != 0 }
}

#[inline]
pub(crate) fn svc1b_mut<T>(
    func: extern "C" fn(&mut T) -> bool,
    arg: &mut T,
    _token: &ThreadToken,
) -> bool {
    unsafe { __svc1(arg as *const T as usize, func as usize) != 0 }
}*/

/*#[inline]
pub(crate) fn svc2n<T1, T2>(
    func: extern "C" fn(&mut T1, &T2),
    arg1: &mut T1,
    arg2: &T2,
    _token: &ThreadToken,
) {
    unsafe {
        __svc2(
            arg1 as *mut T1 as usize,
            arg2 as *const T2 as usize,
            func as usize,
        );
    }
}*/

/*#[inline(always)]
pub(crate) fn svc2n_static_usize<T>(func: extern "C" fn(&'static T, usize), arg1: &'static T, arg2: usize, _token: ThreadToken) {
    unsafe {
        llvm_asm!("
            svc 0
            " :: "{r12}"(func), "{r0}"(arg1), "{r1}"(arg2) :: "volatile");
    }
}*/

/*#[inline]
pub(crate) fn svc2n_box<T1, T2>(
    func: extern "C" fn(&mut T1, Own<T2>),
    arg1: &mut T1,
    arg2: Own<T2>,
    _token: &ThreadToken,
) {
    unsafe {
        __svc2(
            arg1 as *mut T1 as usize,
            arg2.as_ptr() as usize,
            func as usize,
        );
    }
}

#[inline]
pub(crate) fn svc2u<T1, T2>(
    func: extern "C" fn(&mut T1, &T2) -> usize,
    arg1: &mut T1,
    arg2: &T2,
    _token: &ThreadToken,
) -> usize {
    unsafe {
        __svc2(
            arg1 as *mut T1 as usize,
            arg2 as *const T2 as usize,
            func as usize,
        )
    }
}

#[inline]
pub(crate) fn svc2b<T1, T2>(
    func: extern "C" fn(&mut T1, &T2) -> bool,
    arg1: &mut T1,
    arg2: &T2,
    _token: &ThreadToken,
) -> bool {
    unsafe {
        __svc2(
            arg1 as *mut T1 as usize,
            arg2 as *const T2 as usize,
            func as usize,
        ) != 0
    }
}

#[inline]
pub(crate) fn svc2b_mut<T1, T2>(
    func: extern "C" fn(&mut T1, &mut T2) -> bool,
    arg1: &mut T1,
    arg2: &mut T2,
    _token: &ThreadToken,
) -> bool {
    unsafe {
        __svc2(
            arg1 as *mut T1 as usize,
            arg2 as *mut T2 as usize,
            func as usize,
        ) != 0
    }
}*/
