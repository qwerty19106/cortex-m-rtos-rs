use core::ptr::NonNull;

/// Trait fon next item pointer.
///
/// `NextField` is used to create intrusive unbounded containers, such as [Stack](super::Stack), [Queue](super::Queue),
/// et.al. See full list in [common](super) module.
///
/// Intrusive means that item contains pointer to next item.
/// Thus container is not needs dynamic memory to store next item pointer.
/// 
/// Unbounded means that container can takes any number of items.
///
/// # Memory overhead
/// `NextField` intends that struct contains `next: Option<NonNull<Self>>` field.
/// Thus overhead equals `size_of::<usize>() == 4` bytes on item.
/// 
/// # Execution overhead
/// Through `#[inline]` attribute on `next` method one can get zero-cost access to next field.
///
/// # Safety
/// For `let t: Own<T>` variable the `t.next() == None` equality must always be satisfied!
/// It derive than dangling reference not exists.
/// Any container over `T: NextField` struct must gaurantee it!
pub trait NextField {
    /// Get mutable pointer to `next` item
    /// 
    /// It is not unsafe because NonNull require unsafe already.
    fn next(&mut self) -> &mut Option<NonNull<Self>>;
}
