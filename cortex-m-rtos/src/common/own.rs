use core::ops::{Deref, DerefMut};
use core::ptr::NonNull;

/// A pointer type for static allocation with ownership semantic
///
/// `Own<T>` is wrapper around a raw non-null `*mut T`. But unlike `*mut T`, `Own<T>` is covariant over T. It means
/// that no other `*mut T` or `*const T` exists.
/// Thus `Own<T>` has ownership semantic at well as [`Box<T>`](std::boxed::Box<T>).
///
/// # Using
/// `Own` is intended for use with [`Once`](super::Once) and not must be created manually.
///
/// # Dropping
/// `Own` not implement [`Drop`] since static variables are not dropped in Rust.
///
/// # Runtime overhead
/// `Own` is `#[repr(transparent)]`. It provide zero-cost access to T by inline `Deref` and `DerefMut`.
///
/// # Thread safety
/// `Own` is [`Send`] if T is `Send`. Thus `Own` can be sent into other threads.
/// `Own` is not [`Sync`].
///
/// # Memory layout
/// `Option<Own<T>>` has the same size as `Own<T>` and equals `size_of::<usize>() == 4` for supported targets. It
/// allows to optimize memory consumption and to use Own<T> in atomic operations.
///
/// # Copy and Clone
/// `Own` is not `Copy` and `Clone` because `Own<T>` is owning pointer of T.
///
/// # Example
/// ```
/// static DATA: Once<u64> = Once::new(5);
///
/// fn main() {
///     let data = DATA.get();
///     assert!(*data == 5);
/// }
/// ```
#[derive(Debug)]
#[repr(transparent)]
pub struct Own<T: ?Sized>(NonNull<T>);

unsafe impl<T: ?Sized + Send> Send for Own<T> {}

impl<T: ?Sized> Own<T> {
    /// Create Own from raw mut pointer.
    ///
    /// Usually it is used with `Own::as_ptr`. For example, one can store `Own` into
    /// [`AtomicPtr`](core::sync::atomic::AtomicPtr) by `Own::as_ptr` and load then by `Own::new_unchecked`.
    ///
    /// # Safety
    /// The `ptr` should be owning pointer to statically allocated `T`.
    /// Also `ptr` should be aligned on `size_of::<usize>() == 4` bytes!
    #[inline]
    pub unsafe fn new_unchecked(ptr: *mut T) -> Self {
        // SAFETY: owning pointer to statically allocated `T` is non-null.
        Self(unsafe { NonNull::new_unchecked(ptr) })
    }

    /// Convert `Own` to raw mut pointer.
    ///
    /// Usually it is used with `Own::new_unchecked`. For example, one can store `Own` into
    /// [`AtomicPtr`](core::sync::atomic::AtomicPtr) by `Own::as_ptr` and load then by `Own::new_unchecked`.
    #[inline]
    pub fn as_ptr(self) -> *mut T {
        // SAFETY: Own is not Drop. Thus we not call mem::forget.
        self.0.as_ptr()
    }

    /// Convert `Own` to `NonNull`.
    ///
    /// Usually it is used with `Own::from_non_null`. For example, one can store `Own` into some struct (such as
    /// stack) by `Own::as_non_null` and load then by `Own::from_non_null`.
    #[inline]
    pub fn as_non_null(self) -> NonNull<T> {
        // SAFETY: Own is not Drop. Thus we not call mem::forget.
        self.0
    }

    /// Create `Own` from `NonNull<T>`.
    ///
    /// Usually it is used with `Own::from_non_null`. For example, one can store `Own` into some struct (such as
    /// stack) by `Own::as_non_null` and load then by `Own::from_non_null`.
    ///
    /// # Safety
    /// The `non_null` should be owning pointer to statically allocated `T`.
    #[inline]
    pub unsafe fn from_non_null(non_null: NonNull<T>) -> Self {
        Self(non_null)
    }
}

impl<T: ?Sized> Deref for Own<T> {
    type Target = T;

    #[inline]
    fn deref(&self) -> &T {
        unsafe { self.0.as_ref() }
    }
}

impl<T: ?Sized> DerefMut for Own<T> {
    #[inline]
    fn deref_mut(&mut self) -> &mut T {
        unsafe { self.0.as_mut() }
    }
}

// Compile-time tests
mod tests {
    use core::mem::size_of;

    use super::*;

    const _: () = assert!(size_of::<Own<u64>>() == 4);
    const _: () = assert!(size_of::<Option<Own<u64>>>() == 4);
}
