use core::{
    ptr::{null_mut, NonNull},
    sync::atomic::{AtomicPtr, Ordering},
};

use super::{NextField, Own};

/// Stack (LIFO) of T: NextField items
#[derive(Debug)]
#[repr(transparent)]
pub struct ConcurrentStack<T: NextField> {
    /// Pointer to top item of stack
    top: AtomicPtr<T>, // 4 bytes
}

unsafe impl<T: NextField> Sync for ConcurrentStack<T> {}

impl<T: NextField> ConcurrentStack<T> {
    pub const fn new() -> Self {
        Self {
            top: AtomicPtr::new(null_mut()),
        }
    }

    pub fn push(&mut self, mut item: Own<T>) {
        let item_ptr: *mut T = &mut *item;

        let mut top = self.top.load(Ordering::Relaxed);
        loop {
            *item.next() = NonNull::new(top);

            match self.top.compare_exchange_weak(
                top,
                item_ptr,
                Ordering::Relaxed,
                Ordering::Relaxed,
            ) {
                Ok(_) => break,
                Err(top_) => {
                    // Copy current value of self->top to top.
                    top = top_;
                }
            }
        }
    }

    pub fn pop(&mut self) -> Option<Own<T>> {
        let mut top = self.top.load(Ordering::Relaxed);

        loop {
            if top == null_mut() {
                return None; // stack is empty
            }

            // SAFETY: copy pointer to next
            let next = unsafe { (*top).next() };
            let next_ptr = match next {
                None => null_mut(),
                Some(box_) => box_.as_ptr(),
            };

            // Try replace self.top to next
            match self.top.compare_exchange_weak(
                top,
                next_ptr,
                Ordering::Relaxed,
                Ordering::Relaxed,
            ) {
                Ok(_) => {
                    // SAFETY: clear dangling pointer
                    unsafe {
                        *(*top).next() = None;
                    }

                    // SAFETY: Self::push take Own<T>. Here we convert it back.
                    return Some(unsafe { Own::new_unchecked(top) });
                }
                Err(top_) => {
                    // Update top to current value of self.top
                    top = top_;
                }
            }
        }
    }
}
