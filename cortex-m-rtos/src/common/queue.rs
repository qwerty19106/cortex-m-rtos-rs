use core::ptr::NonNull;

use unsafe_unwrap::UnsafeUnwrap;

use super::{NextField, Own};

/// Queue (FIFO) of `T: NextField` items. It is intrusive unbounded container.
///
/// Intrusive means that dynamic allocation is not required.
///
/// Unbounded means that container can takes any number of items.
///
/// # Copy and Clone
/// `Queue` is not `Copy` and `Clone` because `Queue` is owner of all pushed items and `Own<T>` item is not Clone.
#[derive(Debug)]
pub struct Queue<T: NextField> {
    head: Option<NonNull<T>>,
    tail: Option<NonNull<T>>,
}

impl<T: NextField> Queue<T> {
    pub const fn new() -> Self {
        Self {
            head: None,
            tail: None,
        }
    }

    /// Put item to tail of queue
    ///
    /// It transfer ownership of `T` into `Queue<T>`.
    /// `*item.next()` should equals `None` already. See [`NextField`](super::NextField) for details.
    pub fn push(&mut self, item: Own<T>) {
        let non_null = item.as_non_null();

        match &mut self.tail {
            Some(tail_ref) => {
                // `self.tail` is `Some` => `self.head` is `Some`.
                // Thus we must change `self.tail` only.

                // SAFETY: Queue is owner of tail item (Own<T>)
                let tail_ref = unsafe { tail_ref.as_mut() };

                // Set item to self.tail.next()
                *tail_ref.next() = Some(non_null);
            }
            None => {
                // `self.tail` is None => `self.head` is None.
                // Thus we must change `self.tail` and `self.head`.
                self.head = Some(non_null);
            }
        }

        self.tail = Some(non_null);
    }

    /// Pop item from head of queue
    ///
    /// If queue is empty `None` will be returned, otherwise `Some(Own<T>)`.
    pub fn pop(&mut self) -> Option<Own<T>> {
        match &mut self.head {
            None => None,
            Some(head_ref) => {
                let head: NonNull<T> = *head_ref;

                // SAFETY: Queue is owner of head item (Own<T>)
                let head_ref = unsafe { head_ref.as_mut() };

                // Clear self.head.next()
                let next = head_ref.next().take();

                if next.is_none() {
                    // Head is last item in query. Set self.tail to None also.
                    self.tail = None;
                }

                // Set next to self.head
                self.head = next;

                // SAFETY: invariants are provided by self::push API
                Some(unsafe { Own::from_non_null(head) })
            }
        }
    }

    /// Pop item from top of queue without checks
    ///
    /// # Safety
    /// If `self.top` is None then it is `undefined behavior`!
    ///
    /// This method can be used by high level containers, for example `PriorityQueue`.
    pub unsafe fn pop_unchecked(&mut self) -> Own<T> {
        // SAFETY: self.head is not None. See method description
        let head_ref = unsafe { &mut self.head.unsafe_unwrap() };

        let head: NonNull<T> = *head_ref;

        // SAFETY: Queue is owner of head item (Own<T>)
        let head_ref = unsafe { head_ref.as_mut() };

        // Clear self.head.next()
        let next = head_ref.next().take();

        if next.is_none() {
            // Head is last item in query. Set self.tail to None also.
            self.tail = None;
        }

        // Set next to self.head
        self.head = next;

        // SAFETY: invariants are provided by self::push API
        unsafe { Own::from_non_null(head) }
    }

    /// Check that queue not contains items
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.head.is_none()
    }
}
