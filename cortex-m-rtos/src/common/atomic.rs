use core::sync::atomic::AtomicUsize;

/// Atomic increment with limit condition
///
/// Try atomic increment in infinity loop.
/// Return false if value equals limit already. Otherwise value will be incremented and stored, then return true.
///
/// # Safety
/// Overflow is allowed by design.
///
/// # Perfomance (thumbv7em-none-eabihf)
/// Critical section duration (between ldrex and strex/clrex):
/// - success = 3 cycles
/// - limit = 3 cycles
/// - failure attempt = 3 cycles
///
/// Full duration (depending on inline optimization):
/// - success >= 12 cycles
/// - limit >= 8 cycles
/// - failure attempt = 11 cycles
// TODO: estimate cycles for all targets
#[inline]
pub fn atomic_usize_inc_limit(ptr: &AtomicUsize, limit: usize) -> bool {
    let res: usize;

    unsafe {
        asm!("
        1:
            ldrex   {val},[{ptr}]       // 3 cycle
            cmp     {limit},{val}       // 1
            beq     2f                  // 3/1
            add     {val},#1            // 1
            strex   {res},{val},[{ptr}] // 4/2
            cbz     {res},3f            // 3/1
            b       1b                  // 3
        2:
            clrex                       // 1
            mov     {res},#1            // 1
        3:
        ",
        ptr = in(reg_thumb) ptr,
        limit = in(reg_thumb) limit,
        res = lateout(reg_thumb) res,
        val = out(reg_thumb) _,
        options(nostack)
        );
    }

    res == 0
}

/// Atomic increment with limit condition (extended result)
///
/// Try atomic increment in infinity loop.
/// Return (false, old) if value equals limit already. Otherwise value will be incremented and stored, then return
/// (true, old).
///
/// # Safety
/// Overflow is allowed by design.
///
/// # Perfomance (thumbv7em-none-eabihf)
/// Critical section duration (between ldrex and strex/clrex):
/// - success = 3 cycles
/// - limit = 3 cycles
/// - failure attempt = 3 cycles
///
/// Full duration (depending on inline optimization):
/// - success >= 12 cycles
/// - limit >= 8 cycles
/// - failure attempt = 11 cycles
// TODO: estimate cycles for all targets
#[inline]
pub fn atomic_usize_inc_limit_extended(ptr: &AtomicUsize, limit: usize) -> (bool, usize) {
    let res: usize;
    let old: usize;

    unsafe {
        asm!("
        1:
            ldrex   {old},[{ptr}]       // 3 cycle
            cmp     {limit},{old}       // 1
            beq     2f                  // 3/1
            add     {new},{old},#1      // 1
            strex   {res},{new},[{ptr}] // 4/2
            cbz     {res},3f            // 3/1
            b       1b                  // 3
        2:
            clrex                       // 1
            mov     {res},#1            // 1
        3:
        ",
        ptr = in(reg_thumb) ptr,
        limit = in(reg_thumb) limit,
        res = lateout(reg_thumb) res,
        old = out(reg_thumb) old,
        new = out(reg_thumb) _,
        options(nostack)
        );
    }

    ((res == 0), old)
}

/// Atomic decrement with non zero condition
///
/// Try atomic decrement in infinity loop.
/// Return false if value equals 0 already. Otherwise value will be decremented and stored, then return true.
///
/// # Perfomance (thumbv7em-none-eabihf)
/// Critical section duration (between ldrex and strex/clrex):
/// - success = 2 cycles
/// - limit = 3 cycles
/// - failure attempt = 2 cycles
///
/// Full duration (depending on inline optimization):
/// - success >= 11 cycles
/// - limit >= 8 cycles
/// - failure attempt = 10 cycles
// TODO: estimate cycles for all targets
#[inline]
pub fn atomic_usize_dec_non_zero(ptr: &AtomicUsize) -> bool {
    let res: usize;
    let _val: usize;

    unsafe {
        asm!("
        1:
            ldrex   {val},[{ptr}]       // 3 cycle
            cbz     {val},2f            // 3/1
            sub     {val},#1            // 1
            strex   {res},{val},[{ptr}] // 4/2
            cbz     {res},3f            // 3/1
            b       1b                  // 3
        2:
            clrex                       // 1
            mov     {res},#1            // 1
        3:
        ",
        ptr = in(reg_thumb) ptr,
        res = lateout(reg_thumb) res,
        val = out(reg_thumb) _,
        options(nostack)
        );
    }

    res == 0
}
