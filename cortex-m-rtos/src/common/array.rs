use core::{
    ops::{Deref, DerefMut},
    slice::{from_raw_parts, from_raw_parts_mut},
};

use super::Own;

/// A fat pointer type with owning semantic to array.
///
/// RTOS use ArrayBox to erase compile-time array size information, for example in [Thread](crate::os::Thread).
/// ArrayBox<T> has owning semantic at well as [`Own`](super::Own).
/// 
/// # Runtime overhead
/// `ArrayBox` implements `Deref` and `DerefMut` with `#[inline]` attribute.
/// It gives index access to slice of array elements with dynamic index checks on `Debug` profile.
/// In `Release` profile it is zero-cost.
/// 
/// # Thread safety
/// `ArrayBox` is [`Send`] if T is `Send`. Thus `ArrayBox` can be sent into other threads.
/// `ArrayBox` is not [`Sync`].
/// 
/// # Memory layout
/// `size_of::<ArrayBox<T>>() == 8` for supported targets.
/// 
/// # Copy and Clone
/// `ArrayBox` is not `Copy` and `Clone` because `ArrayBox` is owning pointer of T array.
#[derive(Debug)]
pub struct ArrayBox<T> {
    ptr: *mut T,
    size: usize,
}

unsafe impl<T: Send> Send for ArrayBox<T> {}

impl<T> ArrayBox<T> {
    /// Create `ArrayBox<T>` from `Own<[T; const SIZE: usize]>`
    pub fn new<const SIZE: usize>(mut array: Own<[T; SIZE]>) -> Self {
        // Note! We not use mem::forget for array because [T; SIZE] not implement drop.
        Self {
            ptr: &mut array[0],
            size: SIZE,
        }
    }

    // Size of array
    #[inline]
    pub fn size(&self) -> usize {
        self.size
    }
}

impl<T> Deref for ArrayBox<T> {
    type Target = [T];

    #[inline]
    fn deref(&self) -> &[T] {
        // SAFETY: ArrayBox is owner of array. Lifetime will be inherited from ArrayBox reference.
        unsafe { from_raw_parts(self.ptr, self.size) }
    }
}

impl<T> DerefMut for ArrayBox<T> {
    #[inline]
    fn deref_mut(&mut self) -> &mut [T] {
        // SAFETY: ArrayBox is owner of array. Lifetime will be inherited from ArrayBox reference.
        unsafe { from_raw_parts_mut(self.ptr, self.size) }
    }
}
