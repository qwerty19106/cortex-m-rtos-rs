use core::mem::size_of;

const _: () = {
    let count = cfg!(feature = "priority-8") as u8
        + cfg!(feature = "priority-16") as u8
        + cfg!(feature = "priority-32") as u8;

    if count == 0 {
        panic!("One of features should be enabled: priority-8, priority-16, priority-32")
    }

    if count > 1 {
        panic!("Only one of features should be enabled: priority-8, priority-16, priority-32")
    }
};

/// System priority
///
/// `Priority` is wrapped around u8 and have COUNT (8, 16 or 32) variants by design.
/// Limitation of `Priority` inner value is [1..(COUNT-1)]. Value 0 is reserved for idle thread.
///
/// # Runtime overhead
/// This type is zero-cost runtime overhead.
///
/// # Safety
/// The associated constants exists for `priority-8`, `priority-16` and `priority-32` features.
/// Priority(0) can not create by public methods.
#[derive(Clone, Copy, PartialEq, PartialOrd, Debug)]
#[repr(u8)]
pub enum Priority {
    Low = 0,

    #[cfg(all(feature = "priority-8", not(any(feature = "priority-16", feature = "priority-32"))))]
    Low1 = 1,
    #[cfg(all(feature = "priority-8", not(any(feature = "priority-16", feature = "priority-32"))))]
    BelowNormal = 2,
    #[cfg(all(feature = "priority-8", not(any(feature = "priority-16", feature = "priority-32"))))]
    Normal = 3,
    #[cfg(all(feature = "priority-8", not(any(feature = "priority-16", feature = "priority-32"))))]
    AboveNormal = 4,
    #[cfg(all(feature = "priority-8", not(any(feature = "priority-16", feature = "priority-32"))))]
    High = 5,
    #[cfg(all(feature = "priority-8", not(any(feature = "priority-16", feature = "priority-32"))))]
    High1 = 6,
    #[cfg(all(feature = "priority-8", not(any(feature = "priority-16", feature = "priority-32"))))]
    Realtime = 7,

    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    Low1 = 1,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    Low2 = 2,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    BelowNormal = 3,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    BelowNormal1 = 4,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    BelowNormal2 = 5,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    Normal = 6,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    Normal1 = 7,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    Normal2 = 8,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    AboveNormal = 9,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    AboveNormal1 = 10,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    AboveNormal2 = 11,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    High = 12,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    High1 = 13,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    High2 = 14,
    #[cfg(all(feature = "priority-16", not(any(feature = "priority-8", feature = "priority-32"))))]
    Realtime = 15,

    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    Low1 = 1,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    Low2 = 2,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    Low3 = 3,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    Low4 = 4,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    Low5 = 5,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    BelowNormal = 6,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    BelowNormal1 = 7,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    BelowNormal2 = 8,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    BelowNormal3 = 9,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    BelowNormal4 = 10,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    BelowNormal5 = 11,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    Normal = 12,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    Normal1 = 13,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    Normal2 = 14,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    Normal3 = 15,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    Normal4 = 16,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    Normal5 = 17,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    AboveNormal = 18,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    AboveNormal1 = 19,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    AboveNormal2 = 20,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    AboveNormal3 = 21,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    AboveNormal4 = 22,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    AboveNormal5 = 23,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    High = 24,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    High1 = 25,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    High2 = 26,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    High3 = 27,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    High4 = 28,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    High5 = 29,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    Realtime = 30,
    #[cfg(all(feature = "priority-32", not(any(feature = "priority-8", feature = "priority-16"))))]
    Realtime1 = 31,
}

impl Priority {
    #[inline]
    pub(crate) fn to_inner(self) -> u8 {
        self as u8
    }

    /// Count of possible priorities
    pub const COUNT: usize = {
        let mut count = 0;

        if cfg!(feature = "priority-8") {
            count = 8;
        }

        if cfg!(feature = "priority-16") {
            count = 16;
        }

        if cfg!(feature = "priority-32") {
            count = 32;
        }

        count
    };
}

mod tests {
    use super::*;

    const _: () = assert!(size_of::<Priority>() == 1);

    const _: () = assert!(size_of::<Option<Priority>>() == 1);
}
