use crate::common::*;

/// `PriorityQueue` is queue (FIFO) of `T: NextField` items which sorted by priority [`Priority`](super::Priority).
/// It is intrusive unbounded container.
///
/// Intrusive means that dynamic allocation is not required.
///
/// Unbounded means that container can takes any number of items.
///
/// # Runtime overhead
/// `PriorityQueue` contains `COUNT` queues. Each `Queue<T>` corresponds to one `Priority` which is index of Queue in
/// `queues` array.
/// `PriorityQueue` contains `bits` flags field which correspond to `Priority` inner values.
///
/// It allows push item by 2 simple and fast steps:
/// 1) Read and update `bits` flags field,
/// 2) Push item to tail of simple `Queue<T>`.
///
/// Also it allows pop item by 2 simple and fast steps:
/// 1) Get max priority of non-empty `Queue<T>` from `bits` flags field,
/// 2) Pop item from corresponding `Queue<T>`.
///
/// Thus we don't need exhaustive search in a loop in contrast with classic implementation of `PriorityQueue` via
/// `LinkedList`.
/// This algoritm is very fast and performed in a finite number of steps!
/// Wherein we have Priority::COUNT (8, 16 or 32) different values of `Priority` which should be enough to any
/// applied tasks!
///
/// # Memory overhead
/// Size of `PriorityQueue` plus items overhead equals `(Priority::COUNT * 8 + 4) + items_count * 4` bytes.
/// Size of classic intrusive unbounded `LinkedList` equals `4 + items_count * 8` bytes (every item have `prev` and `next` fields).
///
/// Thus for `"priority-32"` feature and 32 items the memory size equals 388 and 260 bytes respectively.
/// Similarly, for `"priority-8"` feature and 8 items the memory size equals 100 and 68 bytes respectively.
///
/// # Copy and Clone
/// `PriorityQueue` not derive `Copy` and `Clone` because `PriorityQueue` is owner of all pushed items, and
/// `Own<T>` item is not Clone.
pub struct PriorityQueue<T: NextField> {
    /// Array of `Queue<T>`
    queues: [Queue<T>; Priority::COUNT],

    /// The `bits` field is bit flags which correspond to non-empty queues in `self.queues`.
    ///
    /// `Priority` values is continuous range [0..(Priority::COUNT - 1)].
    /// Therefore we can use `bits` as bit flags field for non-empty `Queue<T>' in `self.queues`.
    ///
    /// For example priority `let prio = Priority::NORMAL` corresponds to flag `1 << prio.to_inner()`.
    bits: u32,
}

impl<T: NextField> PriorityQueue<T> {
    const _Q: Queue<T> = Queue::new();

    /// Create new PriorityQueue
    pub const fn new() -> Self {
        Self {
            // TODO: переписать на
            // queues: [const { Queue::new() }; Priority::COUNT],
            // Заблокировано на const expressions RFC https://github.com/rust-lang/rust/issues/76001
            queues: [Self::_Q; Priority::COUNT],
            bits: 0,
        }
    }

    /// Push item with some priority
    pub fn push(&mut self, priority: Priority, item: Own<T>) {
        let prio = priority.to_inner() as usize;

        // SAFETY: There is no overflow since prio is in [0..(Priority::COUNT - 1)] range.
        let bit = 1 << prio;

        // Set bit flag
        if self.bits & bit == 0 {
            self.bits = self.bits | bit;
        }

        // SAFETY: Prevent bounds check! Prio is true index for self.queues.
        let queue = unsafe { &mut self.queues.get_unchecked_mut(prio) };

        // Add item to queue
        queue.push(item);
    }

    /// Pop item with max priority
    pub fn pop(&mut self) -> Option<Own<T>> {
        if self.is_empty() {
            None
        } else {
            let lz = self.bits.leading_zeros() as usize;

            // SAFETY: No overflow because self.bits != 0, thus lz < 32! See self.bits field description for details.
            // Prio corresponds to [0..(Priority::COUNT - 1)].
            let prio = 32 - lz - 1;

            // SAFETY: Prevent bounds check! Prio is true index for self.queues.
            let queue = unsafe { &mut self.queues.get_unchecked_mut(prio) };

            // SAFETY: we know that this queue is not empty.
            let res = unsafe { queue.pop_unchecked() };

            if queue.is_empty() {
                // Clear flag for this piority
                self.bits &= !(1 << prio);
            }

            Some(res)
        }
    }

    /// Pop item with some priority
    pub fn pop_by_priority(&mut self, priority: Priority) -> Option<Own<T>> {
        let prio = priority.to_inner() as usize;

        // SAFETY: There is no overflow since prio corresponds to [0..(Priority::COUNT - 1)].
        let bit = 1 << prio;

        if self.bits & bit == 0 {
            None
        } else {
            // SAFETY: Prevent bounds check! Prio is true index for self.queues.
            let queue = unsafe { &mut self.queues.get_unchecked_mut(prio) };

            // SAFETY: we know that this queue is not empty.
            let res = unsafe { queue.pop_unchecked() };

            if queue.is_empty() {
                // Clear flag for this priority
                self.bits &= !(1 << prio);
            }

            Some(res)
        }
    }

    /// Check that `PriorityQueue` is empty
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.bits == 0
    }
}
