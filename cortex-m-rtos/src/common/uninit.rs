use core::mem::MaybeUninit;

use super::Own;

/// Owning pointer to uninitialized T value.
///
/// Usually used to define static uninitialized variable, for example:
/// ```
/// use rust_rtos::common::*;
///
/// static A: Once<Uninit<u8>> = Once::new(Uninit::new());
///
/// fn main() {
///     let a: Own<u8> = A.get().init(5);
///     assert!(*a == 5);
/// }
/// ```
#[repr(transparent)]
pub struct Uninit<T>(MaybeUninit<T>);

impl<T> Uninit<T> {
    pub const fn new() -> Self {
        Self(MaybeUninit::uninit())
    }

    /// Init value and return owning pointer to T
    pub fn init(mut self: Own<Self>, value: T) -> Own<T> {
        let ref_ = self.0.write(value);
    
        // SAFETY: move ownership into return Own
        unsafe { Own::new_unchecked(ref_) }

        // SAFETY: Uninit not implement Drop
    }
}
