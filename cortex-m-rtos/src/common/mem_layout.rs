/// Trait for type with `#repr(transparent)` memory layout and size equals 1, 2 or 4 bytes.
///
/// It is marker that type can be sent into `extern "C"` function as argument, and argument will be placed on 1
/// assembler register. Thus we can use it in assembler code.
/// 
/// It is implemented for:
/// - u8, i8
/// - u16, i16
/// - u32, i32
/// - usize, isize (because `size_of<u32>() == size_of<usize>()`)
/// - `Own` and other OS pointer wrappres
/// 
/// Unsafe because we not can write "where clause" with `#repr(transparent)` and must derive it manually.
// TODO: replace it to where clause when it will be supported by compiler:
// where size_of::<T> == size_of::<u32>()
pub unsafe trait U32OrLessTransparentMemoryLayout {
    /// Get inner value as integer value and then convert into u32 (if needs)
    /// 
    /// It is unsafe because we must gaurantee that result 
    unsafe fn as_u32(self) -> u32;
}
