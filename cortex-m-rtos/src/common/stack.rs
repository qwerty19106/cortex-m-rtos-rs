use core::ptr::NonNull;

use unsafe_unwrap::UnsafeUnwrap;

use super::{NextField, Own};

/// Stack (LIFO) of `T: NextField` items. It is intrusive unbounded container.
///
/// Intrusive means that dynamic allocation is not required.
///
/// Unbounded means that container can takes any number of items.
///
/// # Copy and Clone
/// `Stack` is not `Copy` and `Clone` because `Stack` is owner of all pushed items and `Own<T>` item is not Clone.
#[derive(Debug)]
pub struct Stack<T: NextField> {
    top: Option<NonNull<T>>,
}

impl<T: NextField> Stack<T> {
    pub const fn new() -> Self {
        Self { top: None }
    }

    /// Put item to top of stack
    pub fn push(&mut self, mut item: Own<T>) {
        *item.next() = self.top;
        self.top = Some(item.as_non_null());
    }

    /// Pop item from top of stack
    ///
    /// If stack is empty `None` will be returned, otherwise `Some(Own<T>)`.
    pub fn pop(&mut self) -> Option<Own<T>> {
        match &mut self.top {
            None => None,
            Some(top_ref) => {
                let top: NonNull<T> = *top_ref;

                // SAFETY: Stack is owner of top item (Own<T>)
                let top_ref = unsafe { top_ref.as_mut() };

                // Clear self.top.next
                let next = top_ref.next().take();

                // Set next as top
                self.top = next;

                // SAFETY: invariants are provided by self::push API
                unsafe { Some(Own::from_non_null(top)) }
            }
        }
    }

    /// Pop item from top of stack without checks
    ///
    /// # Safety
    /// If `self.top` is None then it is `undefined behavior`!
    ///
    /// This method can be used by high level containers, for example `PriorityStack`.
    pub unsafe fn pop_unchecked(&mut self) -> Own<T> {
        // SAFETY: top is not None here. See method description!
        let top_ref = unsafe { &mut self.top.unsafe_unwrap() };
        let top = *top_ref;

        // SAFETY: Stack is owner of top item (Own<T>)
        let top_ref = unsafe { top_ref.as_mut() };

        // Clear self.top.next
        let next = top_ref.next().take();

        // Set next as top
        self.top = next;

        // SAFETY: invariants are provided by self::push API
        unsafe { Own::from_non_null(top) }
    }

    /// Check that stack contains items
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.top.is_none()
    }
}
