use core::{
    cell::UnsafeCell,
    sync::atomic::{AtomicBool, Ordering},
};

use super::Own;

/// Statically allocated guard, whose `call` method can be called only once.
///
/// `OnceGuard` is used to construct high level structures, such as [`Once`],
/// [`SemaphoreSA`](crate::primitive::isr::semaphore_sa::SemaphoreSA), et.al.
///
/// # Runtime overhead
/// `OnceGuard` uses atomics for thread-safe protection.
/// Since `OnceGuard::call` must be called only one times (for example, in `main` function) then overhead is insignificant.
///
/// # Thread safety
/// `OnceGuard` is [`Sync`]. It allows to use `OnceGuard` as static variable. Thread-safety is derived by atomic operations.
///
/// # Memory layout
/// `OnceGuard` static variable will be allocated into `.bss` memory region.
/// It allows to decrease used flash memory.
/// For details, see [#53491](https://github.com/rust-lang/rust/issues/53491#issuecomment-420624097) comment.
///
/// Size of `OnceGuard` equals 1 bytes.
///
/// # Example
/// ```
/// // GUARD is placed to .bss memory region
/// static GUARD: OnceGuard = OnceGuard::new();
///
/// fn main() {
///     GUARD.get(); // Success
///     GUARD.get(); // Panic!
/// }
/// ```
#[derive(Debug)]
pub struct OnceGuard(AtomicBool);

unsafe impl Sync for OnceGuard {}

impl OnceGuard {
    pub const fn new() -> Self {
        Self(AtomicBool::new(false))
    }

    /// This method can be called only one times. Otherwise panics.
    pub fn call(&'static self) {
        // SAFETY: Relaxed enought for load self.used
        let used = self.0.load(Ordering::Relaxed);

        if used {
            panic!("OnceGuard called more than one times")
        } else {
            // SAFETY: we store self.used one times, thus Relaxed enought for store and load
            match self
                .0
                .compare_exchange(false, true, Ordering::Relaxed, Ordering::Relaxed)
            {
                Ok(_) => {}
                Err(_) => panic!("OnceGuard called more than one times"),
            }
        }
    }
}

/// Statically allocated wrapper for T which allows to get owning pointer ([Own](super::Own)) of T only one times.
///
/// # Runtime overhead
/// `Once` is not zero-cost wrapper. It use `OnceGuard` for thread-safe protection.
/// Since `Once::get` must be called only one times (for example, in `main` function) then this overhead is insignificant.
///
/// # Thread safety
/// `Once` is [`Sync`]. It allows to use `Once` as static variable. Thread-safety is derived by atomic operations.
///
/// # Memory layout
/// `Once` static variable will be allocated into `.bss` memory region , if value of T is uninitialized or zeroed! It
/// allows to decrease used flash memory.
/// For details, see [#53491](https://github.com/rust-lang/rust/issues/53491#issuecomment-420624097) comment.
///
/// Size of `Once<T>` equals `1 + size_of::<T>()` taking into account the alignment.
/// For example, `size_of::<Once<u32>>() == 8`
///
/// # Example
/// ```
/// // DATA is placed to .bss memory region
/// static DATA: Once<[u8;100]> = Once::new([0; 100]);
///
/// fn main() {
///     let data = DATA.get();
///     assert!(data[0] == 0);
/// }
/// ```
#[derive(Debug)]
pub struct Once<T> {
    value: UnsafeCell<T>,
    guard: OnceGuard,
}

unsafe impl<T> Sync for Once<T> {}

impl<T> Once<T> {
    /// Create new static variable.
    pub const fn new(value: T) -> Self {
        Self {
            value: UnsafeCell::new(value),
            guard: OnceGuard::new(),
        }
    }

    /// Return owning pointer of T only one times.
    pub fn get(&'static self) -> Own<T> {
        // Call guard. Move to infinity loop if failed.
        self.guard.call();

        let ptr = self.value.get();

        // SAFETY: self.guard derive that we is owner of self.value
        // SAFETY: &self.value is not null pointer
        unsafe { Own::new_unchecked(ptr) }
    }
}
