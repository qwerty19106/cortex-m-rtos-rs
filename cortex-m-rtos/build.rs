use std::env;

fn main() {
    let target = env::var("TARGET").unwrap();
    check_target(&target);

    println!("cargo:rerun-if-changed=build.rs");
}

fn check_target(target: &str) {
    if target.starts_with("thumbv7m-") {
        println!("cargo:rustc-cfg=armv7m");
    } else if target.starts_with("thumbv7em-") {
        println!("cargo:rustc-cfg=armv7em");
    } else {
        panic!("Unsupported target {}!", target);
    }

    if target.ends_with("eabihf") {
        println!("cargo:rustc-cfg=has_fpu");
    }
}
